{-# LANGUAGE OverloadedStrings #-}
module Main where

import Wonders.Types
import Wonders.PrettySystem
import Wonders.Interpreter
import Wonders.Utils
import Wonders.Game
import Wonders.Pure

import System.Random
import Control.Lens
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import Control.Monad.Identity
import qualified Data.Foldable as F
import qualified Data.Map.Strict as M
import Data.Monoid
import Data.List (sortBy)
import Data.Ord

pp :: PrettyDoc -> IO ()
pp = print . PP.pretty

askNumber :: PlayerId -> PrettyDoc -> [PrettyDoc] -> IO Int
askNumber "simon" msg choices = do
    pp msg
    forM_ (zip [0..] choices) $ \(i,c) -> pp (numerical (i :: Int) <+> c)
    l <- getLine
    let v = read l
    if all (\x -> x >= '0' && x <= '9') l && v >= 0 && v < length choices
        then return v
        else askNumber "simon" msg choices
askNumber _ _ choices = _doRoll consoleDictionnary (length choices)


consoleDictionnary :: OperationDictionnary IO Identity
consoleDictionnary = OperationDictionnary doroll doask doaskcard getpromise domessage domessageplayer inform (return ())
    where
        inform age turn stt = do
            let (_, evicmap) = runPure (mkStdGen 5) (dummyStrategies $ M.keys (stt ^. players)) stt victoryScore
            case evicmap of
                Left rr -> pp ("Could not compute victory:" <+> rr)
                Right v -> let victoryMap = fmap (F.foldMap Sum) v
                               withState = M.mapWithKey (\k val -> (val, getSum (victoryMap ^. ix k))) (stt ^. players)
                           in  mapM_ pp (showSituation age turn withState)
        doroll mx = getStdRandom (randomR (0,mx - 1))
        doask age pid@"simon" cardlistWpreview curstates =
            case curstates ^. at pid of
                Just mystate -> do
                    let msg = vcat $ "Cartes disponibles:" : map (\(c,e) -> longCard c <+> cardEffectPreview e) cardlistWpreview
                        cardlist = map fst cardlistWpreview
                        actions = allowableActions age pid cardlist curstates
                    n <- askNumber pid msg (map (playerAction mystate cardlist) actions)
                    case actions ^? ix n of
                        Nothing -> error "Can't find action"
                        Just (a,b,_) -> return (Identity (a,b))
                Nothing -> error "could not find player state"
        doask age pid cardlistWpreview curstates =
            let actions = allowableActions age pid cardlist curstates
                cardlist = map fst cardlistWpreview
                cardvalue (CardEffectPreview m v) = fromIntegral v + fromIntegral (m `div` 3)
                actionScore (PlayerAction PlayCard n) = cardvalue $ snd $ cardlistWpreview !! n
                actionScore (PlayerAction DropCard _) = -2 :: Integer
                actionScore _ = 3
                o = sortBy (comparing (\x -> actionScore (x ^. _1))) actions
                (a, b, _) = head $ reverse o
            in  return (Identity (a, b))
        doaskcard pid cards msg = askNumber pid msg (map (\(c,e) -> longCard c <+> cardEffectPreview e) cards)
        getpromise = return . runIdentity
        domessage = pp
        domessageplayer pid m = if pid == "simon"
                                    then print (PP.pretty m)
                                    else return ()

main :: IO ()
main = do
    (_, r) <- runInterpreter consoleDictionnary (dummyState ["simon","pim","pam","poum"]) playGame
    case r of
        Left rr -> pp rr
        Right v -> mapM_ pp (defaultVictoryDisplay v)

