{-# LANGUAGE TupleSections #-}
module Main where

import Wonders.Types
import Wonders.Utils
import Wonders.Game
import Wonders.Pure (runPure, dummyStrategies)
import Crypto.Random
import System.Random
import qualified Data.ByteString as BS
import Control.Lens
import Data.Monoid
import qualified Data.Map.Strict as M
import System.Environment
import Control.Parallel.Strategies
import Data.Tuple (swap)
import Data.List (sort,foldl')

fromRight :: Either a b -> b
fromRight (Right x) = x
fromRight _ = error "fromRight"

getGen :: Int -> IO [StdGen]
getGen nb = do
    rg <- fmap (fst . fromRight . genBytes (4 * nb)) (newGenIO :: IO SystemRandom)
    let mkstdgen =  mkStdGen . BS.foldl (\v n -> v * 256 + fromIntegral n) (0 :: Int)
        splitBS x | BS.null x = []
                  | otherwise = let (a,b) = BS.splitAt 4 x
                                in  a : splitBS b
        rgs = splitBS rg
    return $ map mkstdgen rgs


runGame :: StdGen -> M.Map Wonder (Victory, Int)
runGame rnd =
    let pids = ["a","b","c"]
        (gs, Right vic) = runPure rnd (dummyStrategies pids) (dummyState pids) playGame
        totalvic = vic & traverse %~ getSum . view (traverse . to Sum)
        getWonder pid = case gs ^? players . ix pid . wonder of
                            Just w -> w
                            Nothing -> error "no wonder"
        pmap = M.fromList $ map ( (_1 %~ getWonder) . (_2 %~ (,1)) ) $  M.toList totalvic
    in pmap

main :: IO ()
main = do
    (x:_) <- getArgs
    rnd <- getGen (read x)
    let res = parMap rseq runGame rnd
        mean (v,n) = (fromIntegral v / fromIntegral n) :: Double
    mapM_ print (sort $ map (swap . (_2 %~ mean)) $ M.toList $ foldl' (M.unionWith (\(v1,n1) (v2, n2) -> (v1 + v2, n1 + n2))) mempty res)
