{-# LANGUAGE LambdaCase, QuasiQuotes, OverloadedStrings,
ScopedTypeVariables, DeriveDataTypeable, TemplateHaskell #-}

module Main where

import Backend.Xmpp
import Wonders.Types hiding (Message)
import Wonders.PrettySystem
import Wonders.Interpreter
import Wonders.Utils
import Wonders.Game
import Wonders.Pure (runPure, dummyStrategies)

import System.Environment (getArgs)
import Control.Monad
import Network.Xmpp
import Network
import Data.XML.Types
import Data.Monoid
import Data.String
import Control.Concurrent
import qualified Data.ByteString as BS
import Control.Concurrent.STM
import Control.Lens
import Data.Maybe (fromJust)
import qualified Data.Text as T
import qualified Data.Map.Strict as M
import Crypto.Random
import System.Random

delPlayer :: TVar BotState -> Jid -> IO ()
delPlayer botstate j = atomically $ modifyTVar botstate (bplayers . at (extractName j) .~ Nothing)

addVPlayer :: TVar BotState -> PlayerId -> Maybe Jid -> IO Bool
addVPlayer botstate pid j = join $ atomically $ do
    bs <- readTVar botstate
    if bs ^. curstate /= Joining
        then return (return False)
        else do
            let wasthere = has (bplayers . ix pid) bs
                nbplayers = M.size (bs ^. bplayers) + 1
                playerlist = pid : M.keys (bs ^. bplayers)
            modifyTVar botstate (bplayers . at pid ?~ j)
            return $ if wasthere
                         then return True
                         else errMsg botstate ("Now" <+> emph (numerical nbplayers) <+> "players:" <+> pe (map showPlayerId playerlist)) >> return True

addPlayer :: TVar BotState -> Jid -> IO Bool
addPlayer botstate j = addVPlayer botstate (extractName j) (Just j)

addBot :: TVar BotState -> PlayerId -> IO Bool
addBot botstate botname = addVPlayer botstate botname Nothing

extractName :: Jid -> String
extractName j = case jidToTexts j of
                    (Just "7wonders",_,Just t) -> T.unpack t
                    _ -> show j

reinit :: TVar BotState -> STM ()
reinit = flip modifyTVar ( (curstate   .~ Joining)
                         . (bcurid     .~ Nothing)
                         . (bplayers   .~ mempty)
                         . (bcallbacks .~ mempty)
                         )

runTheGame :: TVar BotState -> [PlayerId] -> IO ()
runTheGame botstate playernames = do
    o <- runInterpreter (mkDictionnary botstate) (dummyState playernames) playGame
    case o of
        (_, Right v) -> mapM_ (errMsg botstate) (defaultVictoryDisplay v)
        (_, Left rr) -> errMsg botstate rr
    atomically (reinit botstate)
    errMsg botstate ("Game finished, type" <+> emph "!ready" <+> "to play another round.")

stopGame :: TVar BotState -> IO ()
stopGame botstate = join $ atomically $ do
    bs <- readTVar botstate
    case (bs ^. curstate, bs ^. bcurid) of
        (Started, Just tid) -> do
            reinit botstate
            return (killThread tid >> errMsg botstate "Game stopped, type !ready to play another round.")
        _ -> return $ errMsg botstate "Game isn't started yet."

startGame :: TVar BotState -> IO ()
startGame botstate = do
    stt <- atomically (readTVar botstate)
    let nbplayers = M.size (stt ^. bplayers)
    if nbplayers >= 3 && nbplayers <= 7
        then do
            errMsg botstate "Starting game !"
            join $ atomically $ do
                bs <- readTVar botstate
                if (bs ^. curstate == Joining)
                    then do
                        modifyTVar botstate (curstate .~ Started)
                        let playernames = M.keys (bs ^. bplayers)
                        return $ do
                            tid <- forkIO $ runTheGame botstate playernames
                            atomically (modifyTVar botstate (bcurid ?~ tid))
                    else return (return ())
        else errMsg botstate ("Can't start the game with" <+> numerical nbplayers <+> "players")

getGen :: IO StdGen
getGen = do
    let fromRight (Right x) = x
        fromRight _ = error "fromRight"
    rg <- fmap (fst . fromRight . genBytes 8) (newGenIO :: IO SystemRandom)
    return $ mkStdGen $ BS.foldl (\v n -> v * 256 + fromIntegral n) (0 :: Int) rg

handleDummy :: TVar BotState -> Message -> [T.Text] -> IO ()
handleDummy tv _ _ = do
    g <- getGen
    let pids = ["pim","pam","poum"]
        (stt, _) = runPure g (dummyStrategies pids) (dummyState pids) playGame
    case stt ^. players . at "pim" of
        Just pst -> mapM_ (errMsg tv) (playerDescLongL pst)
        Nothing -> errMsg tv ":("

handleReady :: TVar BotState -> Message -> [T.Text] -> IO ()
handleReady bs m _ =
    case messageFrom m of
        Just r  -> do
            w <- addPlayer bs r
            errMsg bs $ if w
                            then (fromString (extractName r) <> " added")
                            else (fromString (extractName r) <> " could not be added")
        Nothing -> errMsg bs "error!"

handleAutoplay :: TVar BotState -> Message -> [T.Text] -> IO ()
handleAutoplay bs m _ = adminOnly bs m $ do
    toplay <- fmap (M.elems . _bcallbacks) (atomically (readTVar bs))
    forM_ toplay $ \mv -> putMVar mv "0"

handleReset :: TVar BotState -> Message -> [T.Text] -> IO ()
handleReset bs m _ = adminOnly bs m $ join $ atomically $ do
    tv <- readTVar bs
    if tv ^. curstate == Joining
        then modifyTVar bs (bplayers .~ mempty) >> return (errMsg bs "Players reset")
        else return (errMsg bs "Can't reset when a game is in progress, use !stop")

handleBot :: TVar BotState -> Message -> [T.Text] -> IO ()
handleBot bs _ [] = errMsg bs "Need a name for the bot"
handleBot bs m (n:_) = adminOnly bs m $ do
    let sn = T.unpack n
    w <- addBot bs sn
    errMsg bs $ if w
                    then (str sn <> " added")
                    else (str sn <> " could not be added")

handleLeave :: TVar BotState -> Message -> [T.Text] -> IO ()
handleLeave bs m _ = do
    case messageFrom m of
        Just r  -> do
            delPlayer bs r
            errMsg bs (fromString (extractName r) <> " removed")
        Nothing -> errMsg bs "error!"

handleHelp :: TVar BotState -> Message -> [T.Text] -> IO ()
handleHelp bs _ _ = errMsg bs ("Available commands" <+> pe (map (str . T.unpack) (M.keys commands)))

handleStart :: TVar BotState -> Message -> [T.Text] -> IO ()
handleStart bs m _ = adminOnly bs m (startGame bs)

handleStop :: TVar BotState -> Message -> [T.Text] -> IO ()
handleStop bs m _ = adminOnly bs m (stopGame bs)

adminOnly :: TVar BotState -> Message -> IO () -> IO ()
adminOnly bs m stuff = do
   sess <- getSession bs
   case messageFrom m of
       Just f -> if RUser f `elem` admins
                     then stuff
                     else void $ sendTextContent sess rconfroom "Only admins are allowed to run this command!"
       Nothing -> void $ sendTextContent sess rconfroom "Could not identify the sender."

commands :: M.Map T.Text (TVar BotState -> Message -> [T.Text] -> IO ())
commands = M.fromList [ ("!ready", handleReady )
                      , ("!start", handleStart )
                      , ("!dummy", handleDummy )
                      , ("!leave", handleLeave )
                      , ("!bot",   handleBot )
                      , ("!help" , handleHelp  )
                      , ("!stop" , handleStop  )
                      , ("!reset", handleReset )
                      , ("!autoplay", handleAutoplay )
                      ]

admins :: [Recipient]
admins = map (RUser . fromJust . jidFromText) ["smarechal@interface-tech.com", "7wonders@conference.interface-tech.com/smarechal" ]

rconfroom :: Recipient
rconfroom = RChat (fromJust (jidFromText "7wonders@conference.interface-tech.com"))

main :: IO ()
main = do
    args <- getArgs
    unless (length args == 3) (error "Usage: xmppbot servername username password")
    let [servername,tusername,tpassword] = args
        username = T.pack tusername
        password = T.pack tpassword
    let auth Secured = [scramSha1 username Nothing password, plain username Nothing password]
        auth x = error (show x)
        streamConf = def { connectionDetails = UseHost servername (PortNumber 5222) }
        sessconf = def { sessionStreamConfiguration = streamConf, enableRoster = True }
    sess <- session "interface-tech.com" (Just (auth, Nothing)) sessconf >>= \case
                    Left rr -> error (show rr)
                    Right x -> return x
    rnd <- getGen
    bs <- newTVarIO (BotState mempty Joining sess rnd rconfroom mempty Nothing)
    -- mapM_ (addPlayer bs) playrs
    -- mapM_ (addBot bs) ["pim","pam","poum"]
    sendPresence (presenceOnline { presenceTo = jidFromText "7wonders@conference.interface-tech.com/wonders" }) sess >>= print
    sendTextContent sess rconfroom ("Bot ready, type" <+> emph "!ready" <+> "to start playing.")
    forever $ getMessage sess >>= \msg -> do
        print msg
        let isBody e = nameLocalName (elementName e) == "body"
            content = case filter isBody (messagePayload msg) of
                          (Element _ _ [NodeContent (ContentText t)] : _) -> Just t
                          _ -> Nothing
        case content of
            Nothing -> return ()
            Just cnt -> checkCallBack bs msg >>= \c ->
                case c of
                    Just mv -> putMVar mv cnt
                    Nothing -> checkCommand bs sess msg cnt

checkCallBack :: TVar BotState -> Message -> IO (Maybe (MVar T.Text))
checkCallBack tv msg = case messageFrom msg of
                           Just pjid -> fmap (view (bcallbacks . at pjid)) (atomically (readTVar tv))
                           Nothing -> return Nothing

checkCommand :: TVar BotState -> Session -> Message -> T.Text -> IO ()
checkCommand bs sess msg content = case T.words content of
    (command:parameters) -> when ("!" `T.isPrefixOf` command) $ do
        case M.lookup command commands of
            Just c  -> c bs msg parameters
            Nothing -> answerTextContent sess msg ("Unknown command " <> fromString (T.unpack command))
    _ -> return ()

