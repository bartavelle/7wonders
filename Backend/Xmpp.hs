{-# LANGUAGE OverloadedStrings, TemplateHaskell, DeriveDataTypeable #-}
module Backend.Xmpp where

import Wonders.Types
import Wonders.PrettySystem
import Wonders.Interpreter
import Wonders.Utils
import Wonders.Pure
import Wonders.Game (victoryScore)

import qualified Text.PrettyPrint.ANSI.Leijen as PP
import Data.XML.Types
import Network.Xmpp as XMPP
import Data.Monoid
import qualified Data.Text as T
import qualified Data.Foldable as F
import qualified Data.Map.Strict as M
import Data.Typeable
import Control.Lens
import Control.Monad
import System.Random
import Control.Concurrent
import Control.Concurrent.STM

data Recipient = RUser Jid
               | RChat Jid
               deriving Eq

data BGameState = Joining
                | Started
                deriving Eq

data BotState = BotState { _bplayers   :: M.Map PlayerId (Maybe Jid)
                         , _curstate   :: BGameState
                         , _cursession :: Session
                         , _currand    :: StdGen
                         , _cconfroom  :: Recipient
                         , _bcallbacks :: M.Map Jid (MVar T.Text)
                         , _bcurid     :: Maybe ThreadId
                         } deriving Typeable

makeLenses ''BotState

getSession :: TVar BotState -> IO Session
getSession = fmap (view cursession) . atomically . readTVar

errMsg :: TVar BotState -> PrettyDoc -> IO ()
errMsg stt msg = do
    bs <- atomically (readTVar stt)
    sendTextContent (bs ^. cursession) (bs ^. cconfroom) msg

readCallBack :: TVar BotState -> Jid -> IO T.Text
readCallBack tv pjid = do
    mv <- newEmptyMVar
    atomically $ do
        v <- readTVar tv
        when (has (bcallbacks . ix pjid) v) retry
        modifyTVar tv (bcallbacks . at pjid ?~ mv)
    o <- readMVar mv
    atomically $ modifyTVar tv (bcallbacks . at pjid .~ Nothing)
    return o

askNumber :: TVar BotState -> PlayerId -> PrettyDoc -> [PrettyDoc] -> IO Int
askNumber stt pid msg choices = do
    bs <- atomically (readTVar stt)
    case bs ^. bplayers . at pid of
        Nothing -> errMsg stt ("Could not find" <+> showPlayerId pid) >> return 0
        Just Nothing -> _doRoll (mkDictionnary stt) (length choices)
        Just (Just pjid) -> do
            sendTextContent (bs ^. cursession) (RUser pjid) msg
            forM_ (zip [0..] choices) $ \(i, choice) -> do
                sendTextContent (bs ^. cursession) (RUser pjid) (numerical (i :: Int) <+> choice)
                print (PP.pretty choice)
            v <- readCallBack stt pjid
            if T.all (\x -> x >= '0' && x <= '9') v
                then let n = read (T.unpack v)
                     in  if n >= 0 && n < (length choices)
                             then return n
                             else askNumber stt pid msg choices
                else askNumber stt pid msg choices

mkDictionnary :: TVar BotState -> OperationDictionnary IO MVar
mkDictionnary tv = OperationDictionnary doroll doask doaskcard getpromise domessage domessageplayer inform (return ())
    where
        doroll mx = atomically $ do
            stt <- readTVar tv
            let (v, ns) = randomR (0, mx - 1) (stt ^. currand)
            writeTVar tv (stt & currand .~ ns)
            return v
        doask age pid cardlistWpreview curstates =
            case curstates ^. at pid of
                Just mystate -> do
                    mv <- newEmptyMVar
                    let msg = vcat $ "Cartes disponibles:" : map (\(c,e) -> longCard c <+> cardEffectPreview e) cardlistWpreview
                        cardlist = map fst cardlistWpreview
                        actions = allowableActions age pid cardlist curstates
                    void $ forkIO (askNumber tv pid msg (map (playerAction mystate cardlist) actions) >>= putMVar mv . (\(a,b,_) -> (a,b)) . (actions !!) >> errMsg tv (showPlayerId pid <+> "a joué") )
                    return mv
                Nothing -> errMsg tv ("Could not find player" <+> showPlayerId pid) >> newMVar (PlayerAction DropCard 0, mempty)
        doaskcard pid cards msg = askNumber tv pid msg (map (\(c,e) -> longCard c <+> cardEffectPreview e) cards)
        getpromise = readMVar
        domessage msg = errMsg tv msg
        inform age turn stt = do
            let (_, evicmap) = runPure (mkStdGen 5) (dummyStrategies $ M.keys (stt ^. players)) stt victoryScore
            case evicmap of
                Left rr -> errMsg tv ("Could not compute victory:" <+> rr)
                Right v -> let victoryMap = fmap (F.foldMap Sum) v
                               withState = M.mapWithKey (\k val -> (val, getSum (victoryMap ^. ix k))) (stt ^. players)
                           in  mapM_ (errMsg tv) (showSituation age turn withState)
        domessageplayer pid msg = do
            stt <- atomically (readTVar tv)
            case stt ^. bplayers . at pid of
                Just (Just p) -> sendTextContent (stt ^. cursession) (RUser p) msg
                Just Nothing -> return ()
                Nothing -> errMsg tv ("Could not message" <+> showPlayerId pid)

toNode :: PrettyDoc -> [Node]
toNode = map toNode' . F.toList . _getDoc

withBold :: [Node] -> Node
withBold n = NodeElement (Element {elementName = Name {nameLocalName = "span", nameNamespace = Just "http://www.w3.org/1999/xhtml", namePrefix = Nothing}, elementAttributes = [(Name {nameLocalName = "style",   nameNamespace = Nothing, namePrefix = Nothing},[ContentText "font-weight: bold;"])], elementNodes = n })

toNode' :: PrettyElement -> Node
toNode' (PString s) = NodeContent (ContentText (T.pack s))
toNode' NewLine = NodeElement $ Element (Name "br" (Just "http://www.w3.org/1999/xhtml") Nothing) [] []
toNode' Space = NodeContent (ContentText " ")
toNode' (Emph n) = withBold (toNode n)
toNode' (Colorize c n) = addColor (toColor c) n
    where
        toColor (PCWonder _)            = "#000000"
        toColor (PCCard Marron)         = "#8A5736"
        toColor (PCCard Gris)           = "#9C9C9C"
        toColor (PCCard Jaune)          = "#C9C90C"
        toColor (PCCard Bleu)           = "#0F07ED"
        toColor (PCCard Rouge)          = "#E30000"
        toColor (PCCard Vert)           = "#2fa34a"
        toColor (PCCard Violet)         = "#C429BF"
        toColor (PCCard Builtin)        = "#000000"
        toColor (PCVictory MilitaryV)   = "#FA2020"
        toColor (PCVictory TreasuryV)   = "#FFA600"
        toColor (PCVictory WonderV)     = "#000000"
        toColor (PCVictory CivV)        = "#2600FF"
        toColor (PCVictory ScienceV)    = "#00B506"
        toColor (PCVictory CommercialV) = "#B5A900"
        toColor (PCVictory GuildV)      = "#B5009D"
toNode' (PWonder w) = toNode' (PString (show w))
toNode' (PMoney (Money m)) = if m == 0
                                 then toNode' (PString "")
                                 else addColor "#C9C90C" (numerical m <> "$")
toNode' (PShield (Shield s)) = addColor "#c43131" (str (replicate s '⚔'))
toNode' (PConflict Defaite) = addColor "#c43131" "-1"
toNode' (PConflict (Victoire Age1)) = addColor "#c43131" "+1"
toNode' (PConflict (Victoire Age2)) = addColor "#c43131" "+3"
toNode' (PConflict (Victoire Age3)) = addColor "#c43131" "+5"
toNode' (PWonderStage NotBuilt) = toNode' (PString ".")
toNode' (PWonderStage Stage1) = toNode' (PString "_")
toNode' (PWonderStage Stage2) = toNode' (PString "=")
toNode' (PWonderStage Stage3) = toNode' (PString "Δ")
toNode' (PWonderStage Stage4) = toNode' (PString "☥")
toNode' (PPlayerId p) = toNode' (PString p)
toNode' (Indent _ d) = NodeElement (Element {elementName = Name {nameLocalName = "span", nameNamespace = Just "http://www.w3.org/1999/xhtml", namePrefix = Nothing}, elementAttributes = [(Name {nameLocalName = "margin-left",   nameNamespace = Nothing, namePrefix = Nothing},[ContentText "10px;"])], elementNodes = toNode d })
toNode' (PAge Age1) = toNode' (PString "Ⅰ")
toNode' (PAge Age2) = toNode' (PString "Ⅱ")
toNode' (PAge Age3) = toNode' (PString "Ⅲ")
toNode' (PDirection Droite) = toNode' (PString "▶")
toNode' (PDirection Gauche) = toNode' (PString "◀")
toNode' (PDirection SoiMeme) = toNode' (PString "⇓")
toNode' (ScienceSymbol s) = addColor "#00B506" c
    where
        c = case s of
                Ecrou    -> "⚙"
                Tablette -> "T"
                Compas   -> "☼"
toNode' (PRessource Bois)     = addColor "#008F0C" "B"
toNode' (PRessource Pierre)   = addColor "#BABABA" "P"
toNode' (PRessource Minerais) = addColor "#373737" "M"
toNode' (PRessource Argile)   = addColor "#FF893D" "A"
toNode' (PRessource Papyrus)  = addColor "#D69469" "Y"
toNode' (PRessource Tissus)   = addColor "#F700FF" "T"
toNode' (PRessource Verre)    = addColor "#2028FA" "V"

addColor :: T.Text -> PrettyDoc -> Node
addColor c n = NodeElement (Element {elementName = Name {nameLocalName = "span", nameNamespace = Just "http://www.w3.org/1999/xhtml", namePrefix = Nothing}, elementAttributes = [(Name {nameLocalName = "style", nameNamespace = Nothing, namePrefix = Nothing},[ContentText ("color: " <> c <> ";")])], elementNodes = toNode n })


withFont :: T.Text -> [Node] -> Node
withFont f n = NodeElement (Element {elementName = Name {nameLocalName = "span", nameNamespace = Just "http://www.w3.org/1999/xhtml", namePrefix = Nothing}, elementAttributes = [(Name {nameLocalName = "style", nameNamespace = Nothing, namePrefix = Nothing},[ContentText ("font-family: " <> f <> ";")])], elementNodes = n })


toText :: PrettyDoc -> T.Text
toText = T.pack . flip PP.displayS "" . PP.renderCompact . PP.pretty

toText' :: PrettyElement -> T.Text
toText' = undefined

mkParagraph :: PrettyDoc -> [Element]
mkParagraph x = [ Element {elementName = Name {nameLocalName = "active", nameNamespace = Just "http://jabber.org/protocol/chatstates", namePrefix = Nothing}, elementAttributes = [], elementNodes = []}
                , Element {elementName = Name {nameLocalName = "body", nameNamespace = Just "jabber:client", namePrefix = Nothing}, elementAttributes = [], elementNodes = [NodeContent (ContentText (toText x) )]}
                , Element {elementName = Name {nameLocalName = "html", nameNamespace = Just "http://jabber.org/protocol/xhtml-im", namePrefix = Nothing}, elementAttributes = [], elementNodes =
                    [NodeElement (Element {elementName = Name {nameLocalName = "body", nameNamespace = Just "http://www.w3.org/1999/xhtml", namePrefix = Nothing}, elementAttributes = [], elementNodes =
                                 [ NodeElement (Element {elementName = Name {nameLocalName = "p", nameNamespace = Just "http://www.w3.org/1999/xhtml", namePrefix = Nothing}, elementAttributes = [], elementNodes = toNode x })]})]}]

sendTextContent :: Session -> Recipient -> PrettyDoc -> IO ()
sendTextContent sess rec cnt = do
    let (mjid, msgt) = case rec of
                           RUser j -> (j, Chat)
                           RChat j -> (j, GroupChat)
    print (PP.pretty cnt)
    o <- sendMessage (message { messagePayload = mkParagraph cnt, messageTo = Just mjid, messageType = msgt }) sess
    case o of
        Left rr -> print rr
        Right () -> return ()

answerTextContent :: Session -> XMPP.Message -> PrettyDoc -> IO ()
answerTextContent sess msg cnt =
    case answerMessage msg (mkParagraph cnt) of
        Just m -> do
            o <- sendMessage m sess
            case o of
                Left rr -> print rr
                Right () -> return ()
        Nothing -> error ("Error: could not answer to " ++ show msg ++ ": \n" ++ show (PP.pretty (pe cnt)))
