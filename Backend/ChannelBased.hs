module Backend.ChannelBased where

import Wonders.Interpreter
import Wonders.Types

import qualified Data.Map.Strict as M
import Control.Concurrent
import System.Random

data AskMessage = AskMessage Age PlayerId [(Carte, CardEffectPreview)] (M.Map PlayerId PlayerState) (MVar (PlayerAction, Exchange))
data AskCardMsg = AskCardMsg PlayerId [(Carte, CardEffectPreview)] Message (MVar Int)

data InformationalMessage = Broadcast Message
                          | PlayerMsg PlayerId Message
                          | Informational Age Int GameState
                          | FlushMessage

mkChannelOperationDictionnary :: Chan AskMessage
                              -> Chan AskCardMsg
                              -> Chan InformationalMessage
                              -> OperationDictionnary IO MVar
mkChannelOperationDictionnary askchan askcchan ic = OperationDictionnary roll ask askcard readMVar msg msgp inform flush
    where
        roll mx = getStdRandom (randomR (0,mx - 1))
        ask age pid cardlist pstt = do
            mv <- newEmptyMVar
            writeChan askchan (AskMessage age pid cardlist pstt mv)
            return mv
        askcard pid cardlist m = do
            mv <- newEmptyMVar
            writeChan askcchan (AskCardMsg pid cardlist m mv)
            readMVar mv
        msg = writeChan ic . Broadcast
        msgp pid = writeChan ic . PlayerMsg pid
        inform a t s = writeChan ic (Informational a t s)
        flush = writeChan ic FlushMessage

