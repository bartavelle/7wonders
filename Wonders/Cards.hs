{-# LANGUAGE OverloadedStrings #-}
module Wonders.Cards where

import Wonders.Types
import Data.Monoid
import qualified Data.Set as S

basic :: [Ressource]
basic = [Argile,Bois,Minerais,Pierre]

advanced :: [Ressource]
advanced = [Papyrus,Verre,Tissus]

sbasic,sadvanced :: S.Set Ressource
sbasic = S.fromList basic
sadvanced = S.fromList advanced

getMaxStage :: Wonder -> WonderStage
getMaxStage RhodesB = Stage2
getMaxStage GizehB  = Stage4
getMaxStage  _      = Stage3

getCivilizationResource :: Wonder -> WonderStage -> Carte
getCivilizationResource civ NotBuilt =
    let br t = baseRessource t ("[" <> show civ <> "] base") 0 Age1
    in case civ of
           RhodesA       -> br Minerais
           AlexandrieA   -> br Verre
           EpheseA       -> br Papyrus
           BabyloneA     -> br Argile
           OlympieA      -> br Bois
           HalicarnasseA -> br Tissus
           GizehA        -> br Pierre
           RhodesB       -> br Minerais
           AlexandrieB   -> br Verre
           EpheseB       -> br Papyrus
           BabyloneB     -> br Argile
           OlympieB      -> br Bois
           HalicarnasseB -> br Tissus
           GizehB        -> br Pierre
getCivilizationResource RhodesA Stage1       = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[RhodesA] etape 1"       0 Age1 "BB" [] Builtin
getCivilizationResource AlexandrieA Stage1   = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[AlexandrieA] etape 1"   0 Age1 "PP" [] Builtin
getCivilizationResource EpheseA Stage1       = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[EpheseA] etape 1"       0 Age1 "PP" [] Builtin
getCivilizationResource BabyloneA Stage1     = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[BabyloneA] etape 1"     0 Age1 "AA" [] Builtin
getCivilizationResource OlympieA Stage1      = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[OlympieA] etape 1"      0 Age1 "BB" [] Builtin
getCivilizationResource HalicarnasseA Stage1 = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[HalicarnasseA] etape 1" 0 Age1 "AA" [] Builtin
getCivilizationResource GizehA Stage1        = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[GizehA] etape 1"        0 Age1 "PP" [] Builtin

getCivilizationResource RhodesA Stage2       = Carte [Militaire 2]                             "[RhodesA] etape 2"          0 Age1 "AAA" [] Builtin
getCivilizationResource AlexandrieA Stage2   = Carte [ChoixRessource sbasic Garde]             "[AlexandrieA] etape 2"      0 Age1 "MM"  [] Builtin
getCivilizationResource EpheseA Stage2       = Carte [GagneArgent 9 OccurenceSimple]           "[EpheseA] etape 2"          0 Age1 "BB"  [] Builtin
getCivilizationResource BabyloneA Stage2     = Carte [GuildeScientifiques]                     "[BabyloneA] etape 2"        0 Age1 "BBB" [] Builtin
getCivilizationResource OlympieA Stage2      = Carte [WOlympie (S.fromList [Age1,Age2,Age3])]  "[OlympieA] etape 2"         0 Age1 "PP"  [] Builtin
getCivilizationResource HalicarnasseA Stage2 = Carte [WHalicarnasse]                           "[HalicarnasseA] etape 2"    0 Age1 "MMM" [] Builtin
getCivilizationResource GizehA Stage2        = Carte [GagneVictoire 5 WonderV OccurenceSimple] "[GizehA] etape 2"           0 Age1 "BBB" [] Builtin

getCivilizationResource RhodesA Stage3       = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[RhodesA] etape 3"       0 Age1 "MMMM" [] Builtin
getCivilizationResource AlexandrieA Stage3   = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[AlexandrieA] etape 3"   0 Age1 "VV"   [] Builtin
getCivilizationResource EpheseA Stage3       = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[EpheseA] etape 3"       0 Age1 "YY"   [] Builtin
getCivilizationResource BabyloneA Stage3     = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[BabyloneA] etape 3"     0 Age1 "AAAA" [] Builtin
getCivilizationResource OlympieA Stage3      = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[OlympieA] etape 3"      0 Age1 "MM"   [] Builtin
getCivilizationResource HalicarnasseA Stage3 = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[HalicarnasseA] etape 3" 0 Age1 "TT"   [] Builtin
getCivilizationResource GizehA Stage3        = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[GizehA] etape 3"        0 Age1 "PPPP" [] Builtin

getCivilizationResource RhodesB Stage1       = Carte [GagneVictoire 3 WonderV OccurenceSimple, GagneArgent 3 OccurenceSimple, Militaire 1] "[RhodesB] etape 1" 0 Age1 "PPP" [] Builtin
getCivilizationResource RhodesB Stage2       = Carte [GagneVictoire 4 WonderV OccurenceSimple, GagneArgent 4 OccurenceSimple, Militaire 1] "[RhodesB] etape 2" 0 Age1 "MMMM" [] Builtin
getCivilizationResource RhodesB Stage3       = Carte [] "[RhodesB] etape 3 (invalide)" 0 Age1 "MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM" [] Builtin

getCivilizationResource AlexandrieB Stage1   = Carte [ChoixRessource sbasic Garde]    "[AlexandrieB] etape 1" 0 Age1 "AA" [] Builtin
getCivilizationResource AlexandrieB Stage2   = Carte [ChoixRessource sadvanced Garde] "[AlexandrieB] etape 2" 0 Age1 "BB" [] Builtin
getCivilizationResource AlexandrieB Stage3   = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[AlexandrieB] etape 3"   0 Age1 "PPP"   [] Builtin

getCivilizationResource EpheseB Stage1       = Carte [GagneVictoire 2 WonderV OccurenceSimple, GagneArgent 4 OccurenceSimple] "[EpheseB] etape 1" 0 Age1 "PP" [] Builtin
getCivilizationResource EpheseB Stage2       = Carte [GagneVictoire 3 WonderV OccurenceSimple, GagneArgent 4 OccurenceSimple] "[EpheseB] etape 2" 0 Age1 "BB" [] Builtin
getCivilizationResource EpheseB Stage3       = Carte [GagneVictoire 4 WonderV OccurenceSimple, GagneArgent 4 OccurenceSimple] "[EpheseB] etape 3" 0 Age1 "VTY" [] Builtin

getCivilizationResource BabyloneB Stage1     = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[BabyloneB] etape 1"     0 Age1 "AT" [] Builtin
getCivilizationResource BabyloneB Stage2     = Carte [WJoueDerniereCarte]                      "[BabyloneB] etape 2"     0 Age1 "VBB" [] Builtin
getCivilizationResource BabyloneB Stage3     = Carte [GuildeScientifiques]                     "[BabyloneB] etape 3"     0 Age1 "AAAY" [] Builtin

getCivilizationResource OlympieB Stage1      = Carte [EchangePasCher [Gauche,Droite] sbasic]   "[OlympieB] etape 1" 0 Age1 "BB" [] Builtin
getCivilizationResource OlympieB Stage2      = Carte [GagneVictoire 5 WonderV OccurenceSimple] "[OlympieB] etape 2" 0 Age1 "PP" [] Builtin
getCivilizationResource OlympieB Stage3      = Carte [WCopieGuilde [Gauche,Droite]]            "[OlympieB] etape 3" 0 Age1 "TMM" [] Builtin

getCivilizationResource HalicarnasseB Stage1 = Carte [GagneVictoire 2 WonderV OccurenceSimple, WHalicarnasse] "[HalicarnasseB] etape 1" 0 Age1 "MM" [] Builtin
getCivilizationResource HalicarnasseB Stage2 = Carte [GagneVictoire 1 WonderV OccurenceSimple, WHalicarnasse] "[HalicarnasseB] etape 2" 0 Age1 "AAA" [] Builtin
getCivilizationResource HalicarnasseB Stage3 = Carte [WHalicarnasse]                                          "[HalicarnasseB] etape 3" 0 Age1 "VTY" [] Builtin

getCivilizationResource GizehB Stage1        = Carte [GagneVictoire 3 WonderV OccurenceSimple] "[GizehB] etape 1"        0 Age1 "BB" [] Builtin
getCivilizationResource GizehB Stage2        = Carte [GagneVictoire 5 WonderV OccurenceSimple] "[GizehB] etape 2"        0 Age1 "PPP" [] Builtin
getCivilizationResource GizehB Stage3        = Carte [GagneVictoire 5 WonderV OccurenceSimple] "[GizehB] etape 3"        0 Age1 "AAA" [] Builtin
getCivilizationResource GizehB Stage4        = Carte [GagneVictoire 7 WonderV OccurenceSimple] "[GizehB] etape 4"        0 Age1 "PPPPY" [] Builtin

getCivilizationResource civ Stage4       = Carte [] ("[" <> show civ <> "] etape 4 (invalide)") 0 Age1 "MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM" [] Builtin

baseRessource :: Ressource -> String -> Int -> Age -> Carte
baseRessource r n p a = Carte [GagneRessource r cnt Partage] n p a moneycost [] color
    where color = if isBase r
                      then Marron
                      else Gris
          isBase x = x `elem` basic
          (moneycost, cnt) = if isBase r && a == Age2
                                 then ("$",2)
                                 else ("",1)

resChoice :: Ressource -> Ressource -> String -> Int -> Carte
resChoice r1 r2 n p = Carte [ChoixRessource (S.fromList [r1,r2]) Partage] n p Age1 "$" [] Marron

guildDG :: CouleurCarte -> Victory -> String -> Cost -> Carte
guildDG c nb n cst = Carte [GagneVictoire nb GuildV (PossedeCarte [Gauche,Droite] (S.singleton c))] ("Guilde des " <> n) 0 Age3 cst [] Violet

guilds :: [Carte]
guilds = [ Carte [GagneVictoire 1 GuildV ParDefaite] "Guilde des Stratèges" 100 Age3 "MMPT" [] Violet
         , Carte [GagneVictoire 1 GuildV (PossedeCarte [SoiMeme] (S.fromList [Marron,Gris,Violet]))] "Guilde des Armateurs" 100 Age3 "BBBYV" [] Violet
         , Carte [GuildeScientifiques] "Guilde des Scientifiques" 100 Age3 "BBMMY" [] Violet
         , Carte [GagneVictoire 1 GuildV (ParEtapeMerveille [Gauche,Droite,SoiMeme])] "Guilde des Bâtisseurs" 100 Age3 "PPAAV" [] Violet
         , guildDG Marron 1 "Travailleurs" "MMAPB"
         , guildDG Gris 2 "Artisans" "MMPP"
         , guildDG Jaune 1 "Commerçants" "TYV"
         , guildDG Vert 1 "Philosophes" "AAATY"
         , guildDG Rouge 1 "Espions" "AAAV"
         , guildDG Bleu 1 "Magistrats" "BBBPT"
         ]

allcards :: [Carte]
allcards = [ -- ressources
             baseRessource Bois     "Chantier"    3 Age1
           , baseRessource Bois     "Chantier"    4 Age1
           , baseRessource Pierre   "Cavité"      3 Age1
           , baseRessource Pierre   "Cavité"      5 Age1
           , baseRessource Argile   "Bassin Argileux" 3 Age1
           , baseRessource Argile   "Bassin Argileux" 5 Age1
           , baseRessource Minerais "Filon"       3 Age1
           , baseRessource Minerais "Filon"       4 Age1
           , baseRessource Bois     "Scierie"     3 Age2
           , baseRessource Bois     "Scierie"     4 Age2
           , baseRessource Pierre   "Carrière"    3 Age2
           , baseRessource Pierre   "Carrière"    4 Age2
           , baseRessource Argile   "Briqueterie" 3 Age2
           , baseRessource Argile   "Briqueterie" 4 Age2
           , baseRessource Minerais "Fonderie"    3 Age2
           , baseRessource Minerais "Fonderie"    4 Age2
           , baseRessource Tissus   "Métier à tisser" 3 Age1
           , baseRessource Tissus   "Métier à tisser" 6 Age1
           , baseRessource Tissus   "Métier à tisser" 3 Age2
           , baseRessource Tissus   "Métier à tisser" 5 Age2
           , baseRessource Verre    "Verrerie"        3 Age1
           , baseRessource Verre    "Verrerie"        6 Age1
           , baseRessource Verre    "Verrerie"        3 Age2
           , baseRessource Verre    "Verrerie"        5 Age2
           , baseRessource Papyrus  "Presse"          3 Age1
           , baseRessource Papyrus  "Presse"          6 Age1
           , baseRessource Papyrus  "Presse"          3 Age2
           , baseRessource Papyrus  "Presse"          5 Age2
           , resChoice Bois     Argile   "Friche"     6
           , resChoice Pierre   Argile   "Excavation" 4
           , resChoice Argile   Minerais "Fosse Argileuse" 3
           , resChoice Pierre   Bois     "Exploitation Forestière" 3
           , resChoice Bois     Minerais "Gisement" 5
           , resChoice Minerais Pierre   "Mine" 6
           , Carte [GagneVictoire 3 CivV OccurenceSimple] "Prêteur sur Gages" 4 Age1 "" [] Bleu
           , Carte [GagneVictoire 3 CivV OccurenceSimple] "Prêteur sur Gages" 7 Age1 "" [] Bleu
           , Carte [GagneVictoire 3 CivV OccurenceSimple] "Bains" 3 Age1 "P" ["Aqueduc"] Bleu
           , Carte [GagneVictoire 3 CivV OccurenceSimple] "Bains" 7 Age1 "P" ["Aqueduc"] Bleu
           , Carte [GagneVictoire 5 CivV OccurenceSimple] "Aqueduc" 3 Age2 "PPP" [] Bleu
           , Carte [GagneVictoire 5 CivV OccurenceSimple] "Aqueduc" 7 Age2 "PPP" [] Bleu
           , Carte [GagneVictoire 2 CivV OccurenceSimple] "Autel" 3 Age1 "" ["Temple"] Bleu
           , Carte [GagneVictoire 2 CivV OccurenceSimple] "Autel" 5 Age1 "" ["Temple"] Bleu
           , Carte [GagneVictoire 3 CivV OccurenceSimple] "Temple" 3 Age2 "BAV" ["Panthéon"] Bleu
           , Carte [GagneVictoire 3 CivV OccurenceSimple] "Temple" 6 Age2 "BAV" ["Panthéon"] Bleu
           , Carte [GagneVictoire 7 CivV OccurenceSimple] "Panthéon" 3 Age3 "AAMYTV" [] Bleu
           , Carte [GagneVictoire 7 CivV OccurenceSimple] "Panthéon" 6 Age3 "AAMYTV" [] Bleu
           , Carte [GagneVictoire 2 CivV OccurenceSimple] "Théâtre" 3 Age1 "" ["Statue"] Bleu
           , Carte [GagneVictoire 2 CivV OccurenceSimple] "Théâtre" 6 Age1 "" ["Statue"] Bleu
           , Carte [GagneVictoire 4 CivV OccurenceSimple] "Statue" 3 Age2 "BMM" ["Jardins"] Bleu
           , Carte [GagneVictoire 4 CivV OccurenceSimple] "Statue" 7 Age2 "BMM" ["Jardins"] Bleu
           , Carte [GagneVictoire 5 CivV OccurenceSimple] "Jardins" 3 Age3 "AAB" [] Bleu
           , Carte [GagneVictoire 5 CivV OccurenceSimple] "Jardins" 4 Age3 "AAB" [] Bleu
           , Carte [GagneVictoire 6 CivV OccurenceSimple] "Hôtel de Ville" 3 Age3 "VMPP" [] Bleu
           , Carte [GagneVictoire 6 CivV OccurenceSimple] "Hôtel de Ville" 5 Age3 "VMPP" [] Bleu
           , Carte [GagneVictoire 6 CivV OccurenceSimple] "Hôtel de Ville" 6 Age3 "VMPP" [] Bleu
           , Carte [GagneVictoire 8 CivV OccurenceSimple] "Palace" 3 Age3 "VYTABMP" [] Bleu
           , Carte [GagneVictoire 8 CivV OccurenceSimple] "Palace" 7 Age3 "VYTABMP" [] Bleu
           , Carte [GagneVictoire 4 CivV OccurenceSimple] "Tribunal" 3 Age2 "AAT" [] Bleu
           , Carte [GagneVictoire 4 CivV OccurenceSimple] "Tribunal" 5 Age2 "AAT" [] Bleu
           , Carte [GagneVictoire 6 CivV OccurenceSimple] "Sénat" 3 Age3 "MPBB" [] Bleu
           , Carte [GagneVictoire 6 CivV OccurenceSimple] "Sénat" 5 Age3 "MPBB" [] Bleu
           , Carte [Militaire 1] "Palissade" 3 Age1 "B" [] Rouge
           , Carte [Militaire 1] "Palissade" 7 Age1 "B" [] Rouge
           , Carte [Militaire 1] "Caserne" 3 Age1 "M" [] Rouge
           , Carte [Militaire 1] "Caserne" 5 Age1 "M" [] Rouge
           , Carte [Militaire 1] "Tour de Garde" 3 Age1 "A" [] Rouge
           , Carte [Militaire 1] "Tour de Garde" 4 Age1 "A" [] Rouge
           , Carte [Militaire 2] "Muraille" 3 Age2 "PPP" ["Fortifications"] Rouge
           , Carte [Militaire 2] "Muraille" 7 Age2 "PPP" ["Fortifications"] Rouge
           , Carte [Militaire 2] "Place d'Armes" 4 Age2 "BMM" ["Cirque"] Rouge
           , Carte [Militaire 2] "Place d'Armes" 6 Age2 "BMM" ["Cirque"] Rouge
           , Carte [Militaire 2] "Place d'Armes" 7 Age2 "BMM" ["Cirque"] Rouge
           , Carte [Militaire 3] "Fortifications" 3 Age3 "PMMM" [] Rouge
           , Carte [Militaire 3] "Fortifications" 7 Age3 "PMMM" [] Rouge
           , Carte [Militaire 3] "Cirque" 4 Age3 "PPPM" [] Rouge
           , Carte [Militaire 3] "Cirque" 5 Age3 "PPPM" [] Rouge
           , Carte [Militaire 3] "Cirque" 6 Age3 "PPPM" [] Rouge
           , Carte [Militaire 3] "Arsenal" 3 Age3 "MBBT" [] Rouge
           , Carte [Militaire 3] "Arsenal" 4 Age3 "MBBT" [] Rouge
           , Carte [Militaire 3] "Arsenal" 7 Age3 "MBBT" [] Rouge
           , Carte [Militaire 2] "Écuries" 3 Age2 "MAB" [] Rouge
           , Carte [Militaire 2] "Écuries" 5 Age2 "MAB" [] Rouge
           , Carte [Militaire 2] "Champs de Tir" 3 Age2 "BBM" [] Rouge
           , Carte [Militaire 2] "Champs de Tir" 6 Age2 "BBM" [] Rouge
           , Carte [Militaire 3] "Atelier de Siège" 3 Age3 "BAAA" [] Rouge
           , Carte [Militaire 3] "Atelier de Siège" 5 Age3 "BAAA" [] Rouge
           , Carte [Science Compas] "Officine" 3 Age1 "T" ["Écuries","Dispensaire"] Vert
           , Carte [Science Compas] "Officine" 5 Age1 "T" ["Écuries","Dispensaire"] Vert
           , Carte [Science Compas] "Dispensaire" 3 Age2 "MMV" ["Arène","Loge"] Vert
           , Carte [Science Compas] "Dispensaire" 4 Age2 "MMV" ["Arène","Loge"] Vert
           , Carte [Science Compas] "Loge" 3 Age3 "AATY" [] Vert
           , Carte [Science Compas] "Loge" 6 Age3 "AATY" [] Vert
           , Carte [Science Ecrou] "Atelier" 3 Age1 "V" ["Champs de Tir","Laboratoire"] Vert
           , Carte [Science Ecrou] "Atelier" 7 Age1 "V" ["Champs de Tir","Laboratoire"] Vert
           , Carte [Science Ecrou] "Laboratoire" 3 Age2 "AAY" ["Atelier de Siège","Observatoire"] Vert
           , Carte [Science Ecrou] "Laboratoire" 5 Age2 "AAY" ["Atelier de Siège","Observatoire"] Vert
           , Carte [Science Ecrou] "Observatoire" 3 Age3 "MMVT" [] Vert
           , Carte [Science Ecrou] "Observatoire" 7 Age3 "MMVT" [] Vert
           , Carte [Science Tablette] "Scriptorium" 3 Age1 "Y" ["Tribunal","Bibliothèque"] Vert
           , Carte [Science Tablette] "Scriptorium" 4 Age1 "Y" ["Tribunal","Bibliothèque"] Vert
           , Carte [Science Tablette] "Bibliothèque" 3 Age2 "PPT" ["Sénat","Université"] Vert
           , Carte [Science Tablette] "Bibliothèque" 6 Age2 "PPT" ["Sénat","Université"] Vert
           , Carte [Science Tablette] "Université" 3 Age3 "BBYV" [] Vert
           , Carte [Science Tablette] "Université" 4 Age3 "BBYV" [] Vert
           , Carte [Science Tablette] "Académie" 3 Age3 "PPPV" [] Vert
           , Carte [Science Tablette] "Académie" 7 Age3 "PPPV" [] Vert
           , Carte [Science Ecrou] "Étude" 3 Age3 "BYT" [] Vert
           , Carte [Science Ecrou] "Étude" 5 Age3 "BYT" [] Vert
           , Carte [Science Tablette] "École" 3 Age2 "BY" ["Académie","Étude"] Vert
           , Carte [Science Tablette] "École" 7 Age2 "BY" ["Académie","Étude"] Vert
           , Carte [GagneArgent 5 OccurenceSimple] "Taverne" 4 Age1 "" [] Jaune
           , Carte [GagneArgent 5 OccurenceSimple] "Taverne" 5 Age1 "" [] Jaune
           , Carte [GagneArgent 5 OccurenceSimple] "Taverne" 7 Age1 "" [] Jaune
           , Carte [EchangePasCher [Droite] sbasic] "Comptoir Est" 3 Age1 "$" ["Forum"] Jaune
           , Carte [EchangePasCher [Droite] sbasic] "Comptoir Est" 7 Age1 "$" ["Forum"] Jaune
           , Carte [EchangePasCher [Gauche] sbasic] "Comptoir Ouest" 3 Age1 "$" ["Forum"] Jaune
           , Carte [EchangePasCher [Gauche] sbasic] "Comptoir Ouest" 7 Age1 "$" ["Forum"] Jaune
           , Carte [EchangePasCher [Gauche,Droite] sadvanced] "Marché" 3 Age1 "$" ["Caravansérail"] Jaune
           , Carte [EchangePasCher [Gauche,Droite] sadvanced] "Marché" 6 Age1 "$" ["Caravansérail"] Jaune
           , Carte [ChoixRessource sadvanced Garde] "Forum" 3 Age2 "AA" ["Port"] Jaune
           , Carte [ChoixRessource sadvanced Garde] "Forum" 6 Age2 "AA" ["Port"] Jaune
           , Carte [ChoixRessource sadvanced Garde] "Forum" 7 Age2 "AA" ["Port"] Jaune
           , Carte [ChoixRessource sbasic Garde] "Caravansérail" 3 Age2 "BB" ["Phare"] Jaune
           , Carte [ChoixRessource sbasic Garde] "Caravansérail" 5 Age2 "BB" ["Phare"] Jaune
           , Carte [ChoixRessource sbasic Garde] "Caravansérail" 6 Age2 "BB" ["Phare"] Jaune
           , Carte [GagneArgent 1 (PossedeCarte [Gauche,Droite,SoiMeme] (S.singleton Marron))] "Vignoble" 3 Age2 "" [] Jaune
           , Carte [GagneArgent 1 (PossedeCarte [Gauche,Droite,SoiMeme] (S.singleton Marron))] "Vignoble" 6 Age2 "" [] Jaune
           , Carte [GagneArgent 2 (PossedeCarte [Gauche,Droite,SoiMeme] (S.singleton Gris))] "Bazar" 4 Age2 "" [] Jaune
           , Carte [GagneArgent 2 (PossedeCarte [Gauche,Droite,SoiMeme] (S.singleton Gris))] "Bazar" 7 Age2 "" [] Jaune
           , Carte [GagneArgent 1 (PossedeCarte [SoiMeme] (S.singleton Marron))
           ,GagneVictoire 1 CommercialV (PossedeCarte [SoiMeme] (S.singleton Marron))] "Port" 3 Age3 "TMB" [] Jaune
           , Carte [GagneArgent 1 (PossedeCarte [SoiMeme] (S.singleton Marron))
           ,GagneVictoire 1 CommercialV (PossedeCarte [SoiMeme] (S.singleton Marron))] "Port" 4 Age3 "TMB" [] Jaune
           , Carte [GagneArgent 1 (PossedeCarte [SoiMeme] (S.singleton Jaune))
           ,GagneVictoire 1 CommercialV (PossedeCarte [SoiMeme] (S.singleton Jaune))] "Phare" 3 Age3 "VP" [] Jaune
           , Carte [GagneArgent 1 (PossedeCarte [SoiMeme] (S.singleton Jaune))
           ,GagneVictoire 1 CommercialV (PossedeCarte [SoiMeme] (S.singleton Jaune))] "Phare" 6 Age3 "VP" [] Jaune
           , Carte [GagneArgent 2 (PossedeCarte [SoiMeme] (S.singleton Gris))
           ,GagneVictoire 2 CommercialV (PossedeCarte [SoiMeme] (S.singleton Gris))] "Chambre de Commerce" 4 Age3 "AAY" [] Jaune
           , Carte [GagneArgent 2 (PossedeCarte [SoiMeme] (S.singleton Gris))
           ,GagneVictoire 2 CommercialV (PossedeCarte [SoiMeme] (S.singleton Gris))] "Chambre de Commerce" 6 Age3 "AAY" [] Jaune
           , Carte [GagneArgent 3 (ParEtapeMerveille [SoiMeme])
           ,GagneVictoire 1 CommercialV (ParEtapeMerveille [SoiMeme])] "Arène" 3 Age3 "MPP" [] Jaune
           , Carte [GagneArgent 3 (ParEtapeMerveille [SoiMeme])
           ,GagneVictoire 1 CommercialV (ParEtapeMerveille [SoiMeme])] "Arène" 5 Age3 "MPP" [] Jaune
           , Carte [GagneArgent 3 (ParEtapeMerveille [SoiMeme])
           ,GagneVictoire 1 CommercialV (ParEtapeMerveille [SoiMeme])] "Arène" 7 Age3 "MPP" [] Jaune
           ]

