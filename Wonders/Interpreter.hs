{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module Wonders.Interpreter where

import Wonders.Types

import Control.Monad.Operational
import Control.Monad.State.Strict
import qualified Data.Map.Strict as M

data OperationDictionnary m p = OperationDictionnary { _doRoll          :: Int           -> m Int
                                                     , _doAsk           :: Age           -> PlayerId -> [(Carte, CardEffectPreview)] -> M.Map PlayerId PlayerState -> m (p (PlayerAction, Exchange))
                                                     , _doAskCard       :: PlayerId      -> [(Carte, CardEffectPreview)]  -> Message -> m Int
                                                     , _getPromise      :: forall a. p a -> m a
                                                     , _doMessage       :: Message       -> m ()
                                                     , _doMessagePlayer :: PlayerId      -> Message  -> m ()
                                                     , _doInform        :: Age -> Int -> GameState -> m ()
                                                     , _doflush         :: m ()
                                                     }

runInterpreter :: Monad m
               => OperationDictionnary m p
               -> GameState
               -> GameMonad p a
               -> m (GameState, Either Message a)
runInterpreter dico gamestate m =
    case runState (viewT m) gamestate of
        (!a, !nextstate) -> evalInstrGen dico nextstate a

evalInstrGen :: Monad m
             => OperationDictionnary m p
             -> GameState
             -> ProgramViewT (GameInstr Message p) (State GameState) a
             -> m (GameState, Either Message a)
evalInstrGen _ gamestate (Return x) = return (gamestate, Right x)
evalInstrGen dico gamestate (a :>>= f) =
    let runC a' = runInterpreter dico gamestate (f a')
    in  case a of
            Roll mx                     -> _doRoll dico mx               >>= runC
            AskPlayer age pid clist stt -> _doAsk dico age pid clist stt >>= runC
            AskCard age p m             -> _doAskCard dico age p m       >>= runC
            GetPromise p                -> _getPromise dico p            >>= runC
            GeneralMessage m            -> _doMessage dico m             >>= runC
            PlayerMessage p m           -> _doMessagePlayer dico p m     >>= runC
            Inform age r s              -> _doInform dico age r s        >>= runC
            Flush                       -> _doflush dico                 >>= runC
            Throw e                     -> return (gamestate, Left e)

