{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GADTs #-}
module Wonders.Pure where

import Wonders.Types
import Wonders.Utils
import Wonders.Interpreter
import Wonders.PrettySystem

import Text.PrettyPrint.ANSI.Leijen(pretty)
import Control.Monad.Identity
import Control.Monad.State.Strict
import System.Random
import Control.Applicative

import qualified Data.Map.Strict as M

type Strategy m = Age -> PlayerId -> [Carte] -> M.Map PlayerId PlayerState -> m (PlayerAction, Exchange)

roll :: Int -> State StdGen Int
roll mx = do
    s <- get
    let (v, ns) = randomR (0, mx - 1) s
    put ns
    return v

dummyStrategy :: Strategy (State StdGen)
dummyStrategy age ply cards stt = do
    let actions = allowableActions age ply cards stt
    when (null actions) $ error ("No actions available to me :" ++ show (pretty (map shortCard cards)))
    n <- roll (length actions)
    let (a,b,_) = actions !! n
    return (a,b)

dummyStrategies :: [PlayerId] -> M.Map PlayerId (Strategy (State StdGen))
dummyStrategies = M.fromList . flip zip (repeat dummyStrategy)

runDummy :: [PlayerId] -> GameMonad Identity a -> (GameState, Either Message a)
runDummy pids = runPure (mkStdGen 5) (dummyStrategies pids) (dummyState pids)

pureDict :: M.Map PlayerId (Strategy (State StdGen)) -> OperationDictionnary (State StdGen) Identity
pureDict strats = OperationDictionnary roll aska askc getp amsg pmsg pinf (return ())
    where
        aska age pid clist stt = case M.lookup pid strats of
                                     Just strat -> Identity <$> strat age pid (map fst clist) stt
                                     Nothing -> error ("Could not find strategy for " ++ show pid)
        askc _ crds _ = roll (length crds)
        getp = return . runIdentity
        amsg _ = return ()
        pmsg _ _ = return ()
        pinf _ _ _ = return ()

runPure :: StdGen -- ^ random seed
        -> M.Map PlayerId (Strategy (State StdGen))
        -> GameState
        -> GameMonad Identity a
        -> (GameState, Either Message a)
runPure seed strategies gamestate m = evalState (runInterpreter (pureDict strategies) gamestate m) seed

