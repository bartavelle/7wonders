{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ConstraintKinds #-}

module Wonders.Types where

import Data.String
import Data.Monoid
import Data.Foldable (foldMap)
import qualified Data.Map.Strict as M
import Control.Monad.Operational
import Control.Monad.State.Strict
import Control.Monad.Error.Class
import Control.Lens
import Control.Applicative
import qualified Data.Set as S
import qualified Data.MultiSet as MS
import qualified Data.Sequence as Seq

data SpecialActionInformation = UsedOlymp
                              deriving (Show, Eq)

data PrettyElement = PWonder Wonder
                   | PMoney Money
                   | PShield Shield
                   | PConflict JetonConflit
                   | PWonderStage WonderStage
                   | PPlayerId PlayerId
                   | PString String
                   | PRessource Ressource
                   | Space
                   | NewLine
                   | Emph PrettyDoc
                   | Colorize PColour PrettyDoc
                   | Indent Int PrettyDoc
                   | PAge Age
                   | PDirection DirectionEffet
                   | ScienceSymbol TypeScience
                   deriving Eq

data PColour = PCWonder Wonder
             | PCCard CouleurCarte
             | PCVictory VictoryCategory
             deriving Eq

newtype PrettyDoc = PrettyDoc { _getDoc :: Seq.Seq PrettyElement }
                  deriving Eq

instance IsString PrettyDoc where
    fromString = PrettyDoc . Seq.singleton . PString

instance Monoid PrettyDoc where
    mempty = PrettyDoc Seq.empty
    mappend (PrettyDoc a) (PrettyDoc b) = PrettyDoc (mappend a b)

instance Error PrettyDoc where
    noMsg = mempty
    strMsg = fromString

newtype Money = Money { getMoney :: Int }
    deriving (Show, Ord, Eq, Num, Integral, Real, Enum)

newtype Shield = Shield { getShield :: Int }
    deriving (Show, Ord, Eq, Num, Integral, Real, Enum)

newtype Victory = Victory { getVictory :: Int }
    deriving (Show, Ord, Eq, Num, Integral, Real, Enum)

data Age = Age1 | Age2 | Age3
         deriving (Show, Eq, Ord)

data JetonConflit = Defaite
                  | Victoire Age
                  deriving (Show, Eq)

data Ressource = Verre | Tissus | Papyrus | Argile | Bois | Minerais | Pierre
               deriving (Show, Eq, Ord)

data DirectionEffet = Droite
                    | Gauche
                    | SoiMeme
                    deriving (Show, Eq, Ord)

type Cible = [DirectionEffet]

-- une condition se transforme en int, pour savoir combien de fois elle
-- a matché
data Condition = OccurenceSimple -- pour les trucs qui arrivent une fois
               | PossedeCarte Cible (S.Set CouleurCarte)
               | ParDefaite
               | ParEtapeMerveille Cible
               deriving (Show, Eq, Ord)

data CouleurCarte = Marron | Gris | Jaune | Bleu | Vert | Rouge | Violet | Builtin
                  deriving (Show, Eq, Ord)

data VictoryCategory = MilitaryV | TreasuryV | WonderV | CivV | ScienceV | CommercialV | GuildV
                     deriving (Show, Eq, Ord)

data RessourcePartage = Partage | Garde
                      deriving (Show, Eq, Ord)

data CardEffect = GagneRessource Ressource Int RessourcePartage
                | ChoixRessource (S.Set Ressource) RessourcePartage
                | EchangePasCher Cible (S.Set Ressource)
                | GagneVictoire Victory VictoryCategory Condition
                | GagneArgent Money Condition
                | Science TypeScience
                | Militaire Shield
                | GuildeScientifiques
                | WHalicarnasse
                | WOlympie (S.Set Age)
                | WJoueDerniereCarte
                | WCopieGuilde Cible
                deriving (Show, Eq, Ord)

data TypeScience = Compas | Tablette | Ecrou
                 deriving (Show, Eq, Ord)

data Cost = Cost (MS.MultiSet Ressource) Money
          deriving (Show, Eq, Ord)

data Carte = Carte { _ceffet   :: [CardEffect]
                   , _cnom     :: String
                   , _cjoueurs :: Int
                   , _cage     :: Age
                   , _ccost    :: Cost
                   , _cgratuit :: [String]
                   , _ccouleur :: CouleurCarte
                   } deriving ( Show,Eq, Ord )

instance Monoid Cost where
    mempty = Cost mempty 0
    mappend (Cost r1 m1) (Cost r2 m2) = Cost (r1<>r2) (m1+m2)

instance IsString Cost where
    fromString = foldMap toCost
        where
            toCost 'A' = Cost (MS.singleton Argile) 0
            toCost 'B' = Cost (MS.singleton Bois) 0
            toCost 'P' = Cost (MS.singleton Pierre) 0
            toCost 'M' = Cost (MS.singleton Minerais) 0
            toCost 'V' = Cost (MS.singleton Verre) 0
            toCost 'T' = Cost (MS.singleton Tissus) 0
            toCost 'Y' = Cost (MS.singleton Papyrus) 0
            toCost '$' = Cost mempty 1
            toCost x = error ("Can't decode " <> show x)

type PlayerId = String

dummyPlayer :: PlayerId
dummyPlayer = "dummy"

data Wonder = RhodesA
            | AlexandrieA
            | EpheseA
            | BabyloneA
            | OlympieA
            | HalicarnasseA
            | GizehA
            | RhodesB
            | AlexandrieB
            | EpheseB
            | BabyloneB
            | OlympieB
            | HalicarnasseB
            | GizehB
            deriving (Show,Eq,Ord,Enum)

data WonderStage = NotBuilt | Stage1 | Stage2 | Stage3 | Stage4
                 deriving (Show,Eq,Enum)

data PlayerState = PlayerState { _wonder      :: Wonder
                               , _curmoney    :: Money
                               , _curconflict :: [JetonConflit]
                               , _wonderStage :: WonderStage
                               , _leftNeigh   :: PlayerId
                               , _rightNeigh  :: PlayerId
                               , _cardsInGame :: [Carte]
                               }
                               deriving (Show,Eq)

data GameState = GameState { _dealtMap  :: M.Map PlayerId [Carte]
                           , _players   :: M.Map PlayerId PlayerState
                           , _discarded :: [Carte]
                           }
               deriving (Show, Eq)

data PlayerAction = PlayerAction PlayerActionType Int
                  deriving (Show, Eq)

data PlayerActionType = PlayCard
                      | DropCard
                      | BuildWonder
                      deriving (Show, Eq)

data Voisin = VDroite | VGauche
            deriving (Show, Eq, Ord)

type Exchange = M.Map Voisin (MS.MultiSet Ressource)

type Message = PrettyDoc

data CardEffectPreview = CardEffectPreview Money Victory

instance Monoid CardEffectPreview where
    mempty = CardEffectPreview 0 0
    (CardEffectPreview m1 v1) `mappend` (CardEffectPreview m2 v2) = CardEffectPreview (m1+m2) (v1+v2)

data GameInstr e promise a where
    Roll            :: Int                                                -> GameInstr e promise Int
    AskPlayer       :: Age -> PlayerId  -> [(Carte, CardEffectPreview)] -> M.Map PlayerId PlayerState -> GameInstr e promise (promise (PlayerAction, Exchange))
    AskCard         :: PlayerId  -> [(Carte, CardEffectPreview)] -> Message                    -> GameInstr e promise Int
    GetPromise      :: promise a                                          -> GameInstr e promise a
    GeneralMessage  :: Message                                            -> GameInstr e promise ()
    PlayerMessage   :: PlayerId  -> Message                               -> GameInstr e promise ()
    Throw           :: e                                                  -> GameInstr e promise a
    Inform          :: Age -> Int -> GameState                            -> GameInstr e promise ()
    Flush           :: GameInstr e promise ()

type GameMonad promise = ProgramT (GameInstr Message promise) (State GameState)
type MConstraint m = (MonadError PrettyDoc m, MonadState GameState m, Monad m, Functor m, Applicative m)

instance (Monad m, Error e) => MonadError e (ProgramT (GameInstr e promise) m) where
    throwError = singleton . Throw
    catchError = error "catchError not implemented"

makeClassy ''PlayerState
makeClassy ''GameState
makeClassy ''Carte
makePrisms ''CouleurCarte
makePrisms ''CardEffect

