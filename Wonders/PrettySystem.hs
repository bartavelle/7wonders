{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
module Wonders.PrettySystem where

import Wonders.Types
import Wonders.Utils

import qualified Data.Sequence as Seq
import Data.Monoid
import qualified Data.Foldable as F
import Data.List (intersperse)
import Data.String
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import qualified Data.Map.Strict as M
import Control.Lens
import Data.List (sort)

class PrettyE e where
    pe :: e -> PrettyDoc

instance (PrettyE a, F.Foldable f) => PrettyE (f a) where
    pe l = brackets $ sepBy (pchar ',') (map pe (F.toList l))

instance PrettyE PrettyDoc where
    pe = id
instance PrettyE Wonder where
    pe = PrettyDoc . Seq.singleton . PWonder
instance PrettyE Money where
    pe = PrettyDoc . Seq.singleton . PMoney
instance PrettyE JetonConflit where
    pe = PrettyDoc . Seq.singleton . PConflict
instance PrettyE WonderStage where
    pe = PrettyDoc . Seq.singleton . PWonderStage
instance PrettyE Ressource where
    pe = PrettyDoc . Seq.singleton . PRessource
instance PrettyE Age where
    pe = PrettyDoc . Seq.singleton . PAge
instance PrettyE DirectionEffet where
    pe = PrettyDoc . Seq.singleton . PDirection
instance PrettyE TypeScience where
    pe = PrettyDoc . Seq.singleton . ScienceSymbol
instance PrettyE Shield where
    pe = PrettyDoc . Seq.singleton . PShield
instance PrettyE Voisin where
    pe VGauche = pe Gauche
    pe VDroite = pe Droite

victory :: Victory -> VictoryCategory -> PrettyDoc
victory v vc = PrettyDoc (Seq.singleton (Colorize (PCVictory vc) (numerical v)))

brackets :: PrettyDoc -> PrettyDoc
brackets e = pchar '[' <> e <> pchar ']'

space :: PrettyDoc
space = PrettyDoc (Seq.singleton Space)

sepBy :: F.Foldable f => PrettyDoc -> f PrettyDoc -> PrettyDoc
sepBy sep = mconcat . intersperse sep . F.toList

pchar :: Char -> PrettyDoc
pchar = PrettyDoc . Seq.singleton . PString . (:[])

withCardColor :: CouleurCarte -> PrettyDoc -> PrettyDoc
withCardColor c = PrettyDoc . Seq.singleton . Colorize (PCCard c)

withVictoryColor :: VictoryCategory -> PrettyDoc -> PrettyDoc
withVictoryColor v = PrettyDoc . Seq.singleton . Colorize (PCVictory v)

numerical :: Integral n => n -> PrettyDoc
numerical = fromString . (show :: Integer -> String) . fromIntegral

showPlayerId :: PlayerId -> PrettyDoc
showPlayerId = PrettyDoc . Seq.singleton . PPlayerId

str :: String -> PrettyDoc
str = PrettyDoc . Seq.singleton . PString

emph :: PrettyDoc -> PrettyDoc
emph = PrettyDoc . Seq.singleton . Emph

pcost :: Cost -> PrettyDoc
pcost (Cost r m) = F.foldMap pe r <> pe m

indent :: Int -> PrettyDoc -> PrettyDoc
indent d = PrettyDoc . Seq.singleton . Indent d

(<+>) :: PrettyDoc -> PrettyDoc -> PrettyDoc
a <+> b = a <> PrettyDoc (Seq.singleton Space) <> b

(</>) :: PrettyDoc -> PrettyDoc -> PrettyDoc
a </> b = a <> newline <> b

newline :: PrettyDoc
newline = PrettyDoc (Seq.singleton NewLine)

vcat :: [PrettyDoc] -> PrettyDoc
vcat = mconcat . intersperse newline

cardName :: Carte -> PrettyDoc
cardName c = withCardColor (c ^. ccouleur) (fromString (c ^. cnom))

shortCard :: Carte -> PrettyDoc
shortCard c = cardName c <+> pcost (c ^. ccost) <+> pe (map cardEffectShort (c ^. ceffet))

longCard :: Carte -> PrettyDoc
longCard c = shortCard c <+> brackets ( pe (c ^. cage) )
                         <+> if null grt
                                 then mempty
                                 else "- Gratuit:" <+> pe grt
    where grt = c ^.. cgratuit . traverse . to str

cardEffectShort :: CardEffect -> PrettyDoc
cardEffectShort (GagneRessource r n _) = "+" <> mconcat (replicate n (pe r))
cardEffectShort (ChoixRessource rs _) = "+" <> sepBy "/" (map pe (F.toList rs))
cardEffectShort (EchangePasCher t rs) = "Echange" <+> F.foldMap pe (F.toList rs) <+> F.foldMap pe t
cardEffectShort (GagneVictoire v vc cond) = "+" <> victory v vc <+> conditionShort cond
cardEffectShort (GagneArgent m cond) = "+" <> pe m <+> conditionShort cond
cardEffectShort (Science s) = pe s
cardEffectShort (Militaire m) = "+" <> pe m
cardEffectShort GuildeScientifiques = sepBy "/" (map pe [Compas, Tablette, Ecrou])
cardEffectShort WHalicarnasse = "Joue une carte défaussée gratuitement au moment où la capacité est jouée."
cardEffectShort (WOlympie _) = "Fabrique un bâtiment gratuitement par âge."
cardEffectShort (WCopieGuilde t) = "Peut copier une guilde " <+> brackets (F.foldMap pe t)
cardEffectShort WJoueDerniereCarte = "Vous pouvez jouer la dernière carte de chaque tour au lieu de la jeter."

conditionShort :: Condition -> PrettyDoc
conditionShort OccurenceSimple = mempty
conditionShort ParDefaite = "defaite"
conditionShort (ParEtapeMerveille t) = brackets (F.foldMap pe t)
conditionShort (PossedeCarte t c) = sepBy "/" $ F.foldMap pe t : map (\cc -> withCardColor cc (str (show cc))) (F.toList c)

playerDescLongL :: PlayerState -> [PrettyDoc]
playerDescLongL p = playerDescShort p : map (indent 4 . shortCard) (p ^. cardsInGame)

playerDescLong :: PlayerState -> PrettyDoc
playerDescLong = vcat . playerDescLongL

playerDescShort :: PlayerState -> PrettyDoc
playerDescShort p@(PlayerState w m _ ws lp rp _) = brackets (pe w <> "-" <> pe ws) <+> brackets (pe Gauche <> showPlayerId lp <+> showPlayerId rp <> pe Droite) <+> pe m <+> availableresources <+> milit <+> science
    where
        availableresources = dispo <+> multi
        dispo = F.foldMap pe $ sort $ concatMap (\(r,n,_) -> replicate n r) $ p ^.. playerEffects . _GagneRessource
        multi = F.foldMap (brackets . F.foldMap pe . F.toList) $ p ^.. playerEffects . _ChoixRessource . _1
        milit = pe $ sum $ p ^.. playerEffects . _Militaire
        science = F.foldMap pe $ sort $ p ^.. playerEffects . _Science

showSituation :: Age -> Int -> M.Map PlayerId (PlayerState, Victory) -> [PrettyDoc]
showSituation age turn stt = hdr : map (\(n,(ps,v)) -> showPlayerId n <+> brackets (victory v CivV <> "♚") <+> playerDescShort ps) (M.toList stt)
    where
        hdr = "Age" <+> pe age <+> ", turn" <+> numerical turn

playerAction :: PlayerState -> [Carte] -> (PlayerAction, Exchange, Maybe SpecialActionInformation) -> PrettyDoc
playerAction mystate cardlist (acti, exc, spc) = da acti <+> F.foldMap de (M.toList exc) <+> ds spc
    where
        getCard i = case cardlist ^? ix i of
                        Just c  -> cardName c
                        Nothing -> "???"
        da (PlayerAction PlayCard i) = withCardColor Bleu  "Jouer" <+> getCard i
        da (PlayerAction DropCard i) = withCardColor Rouge "Jeter" <+> getCard i
        da (PlayerAction BuildWonder i) = withCardColor Violet "Fabriquer sa merveille" <+> "avec" <+> getCard i
        de (v, exch) =
            let cost = getExchangeCost mystate v exch
            in  "échange" <+> F.foldMap pe (F.toList exch) <+> "avec" <+> neigh v <+> "pour" <+> pe cost
        neigh VGauche = pe VGauche <> brackets (showPlayerId (mystate ^. leftNeigh))
        neigh VDroite = pe VDroite <> brackets (showPlayerId (mystate ^. rightNeigh))
        ds (Just UsedOlymp) = "(en utilisant la capacité spéciale Olympe)"
        ds Nothing = mempty

defaultVictoryDisplay :: M.Map PlayerId (M.Map VictoryCategory Victory) -> [PrettyDoc]
defaultVictoryDisplay = map dsplyline . M.toList
    where
        dsplyline (pid, vic) = showPlayerId pid <+> brackets (victory total CivV <> "♚") <+> pe (map (\(k,v) -> victory v k) (M.toList vic))
            where
                total = getSum $ F.foldMap Sum vic

cardEffectPreview :: CardEffectPreview -> PrettyDoc
cardEffectPreview (CardEffectPreview 0 0) = mempty
cardEffectPreview (CardEffectPreview m 0) = brackets (pe m)
cardEffectPreview (CardEffectPreview 0 v) = brackets (victory v CivV)
cardEffectPreview (CardEffectPreview m v) = brackets (pe m <> "/" <> victory v CivV)

instance PP.Pretty PrettyDoc where
    pretty = F.foldMap PP.pretty . _getDoc

instance PP.Pretty PrettyElement where
    pretty (PWonder w) = PP.string (show w)
    pretty (PMoney m) | m == 0    = mempty
                      | otherwise = PP.yellow (PP.string (show (getMoney m) <> "$"))
    pretty (PShield (Shield m)) | m == 0    = mempty
                                | otherwise = PP.red $ PP.string $ if m > 6
                                                                       then show m <> "⚔"
                                                                       else replicate m '⚔'
    pretty (PConflict Defaite) = PP.dullred "-1"
    pretty (PConflict (Victoire Age1)) = PP.dullred "+1"
    pretty (PConflict (Victoire Age2)) = PP.dullred "+3"
    pretty (PConflict (Victoire Age3)) = PP.dullred "+5"
    pretty (PWonderStage NotBuilt) = PP.dullwhite  "."
    pretty (PWonderStage Stage1)   = PP.white      "_"
    pretty (PWonderStage Stage2)   = PP.dullyellow "="
    pretty (PWonderStage Stage3)   = PP.yellow     "Δ"
    pretty (PWonderStage Stage4)   = PP.yellow     "☥"
    pretty (PPlayerId p) = PP.bold (PP.string p)
    pretty (PRessource Verre)    = PP.dullcyan   "V"
    pretty (PRessource Tissus)   = PP.dullwhite  "T"
    pretty (PRessource Papyrus)  = PP.magenta    "Y"
    pretty (PRessource Argile)   = PP.dullyellow "A"
    pretty (PRessource Bois)     = PP.dullgreen  "B"
    pretty (PRessource Minerais) = PP.dullwhite  "M"
    pretty (PRessource Pierre)   = PP.white      "P"
    pretty Space = PP.space
    pretty NewLine = PP.linebreak
    pretty (Emph doc) = PP.bold (PP.pretty doc)
    pretty (Colorize (PCVictory c) d) = clr (PP.pretty d)
        where
            clr = case c of
                      MilitaryV   -> PP.red
                      TreasuryV   -> PP.dullyellow
                      WonderV     -> id
                      CivV        -> PP.dullblue
                      ScienceV    -> PP.dullgreen
                      CommercialV -> PP.dullred
                      GuildV      -> PP.magenta
    pretty (Colorize (PCCard c) d) = clr (PP.pretty d)
        where
            clr = case c of
                      Marron  -> id
                      Gris    -> id
                      Jaune   -> PP.dullyellow
                      Bleu    -> PP.dullcyan
                      Vert    -> PP.green
                      Rouge   -> PP.red
                      Violet  -> PP.magenta
                      Builtin -> id
    pretty (Colorize _ d) = PP.pretty d
    pretty (Indent n d) = PP.indent n (PP.pretty d)
    pretty (PAge Age1) = "Ⅰ"
    pretty (PAge Age2) = "Ⅱ"
    pretty (PAge Age3) = "Ⅲ"
    pretty (PDirection Droite) = "▶"
    pretty (PDirection Gauche) = "◀"
    pretty (PDirection SoiMeme) = "⇓"
    pretty (ScienceSymbol b) = PP.cyan $ PP.string $ case b of
                                                         Ecrou    -> "⚙"
                                                         Tablette -> "T"
                                                         Compas   -> "☼"
    pretty (PString s) = PP.string s
