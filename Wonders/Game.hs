{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ConstraintKinds #-}

module Wonders.Game where

import Wonders.Types
import Wonders.Utils
import Wonders.Cards
import Wonders.PrettySystem

import Control.Monad.Operational (singleton)
import Control.Applicative
import Data.Monoid
import Data.List.Split
import Control.Monad.Error.Class
import Control.Monad
import Control.Lens

import qualified Data.Set as S
import qualified Data.Map.Strict as M
import qualified Data.MultiSet as MS

scienceScore :: [TypeScience] -> Int -> Victory
scienceScore tp j
    | j == 0    = score tp
    | otherwise = maximum (map (\t -> scienceScore (t:tp) (j-1)) alltypes)
    where
        alltypes = [Compas, Tablette, Ecrou]
        score :: [TypeScience] -> Victory
        score n =
            let eachtype = map (\t -> length (filter (== t) n)) alltypes
            in  Victory (sum (map (\x -> x * x) eachtype) + 7 * minimum eachtype)

getCardVictory :: MConstraint m => PlayerId -> Carte -> m [(VictoryCategory, Victory)]
getCardVictory pid card = mapM getCardVictory' (card ^.. ceffet . traverse . _GagneVictoire)
    where
        getCardVictory' (pts, cat, cond) = do
            tgts <- getNbTargets pid cond
            return (cat, pts * fromIntegral tgts)


victoryScore :: MConstraint m => m (M.Map PlayerId (M.Map VictoryCategory Victory))
victoryScore = use players >>= itraverse computeScore
 -- use players >>= traverse computeScore
    where
        computeScore pid stt = do
            let milscore = (MilitaryV, sum (map getMilitaryScore (stt ^. curconflict)))
                moneyscore = (TreasuryV, Victory ( getMoney (stt ^. curmoney) `div` 3 ))
                sciencescore = (ScienceV, scienceScore (stt ^.. playerEffects . _Science) (length (stt ^.. playerEffects . _GuildeScientifiques)))
                getMilitaryScore Defaite = -1
                getMilitaryScore (Victoire Age1) = 1
                getMilitaryScore (Victoire Age2) = 3
                getMilitaryScore (Victoire Age3) = 5
            cardPoints <- mapM (getCardVictory pid) (stt ^. cardsInGame)
            return (M.fromListWith (+) ( milscore : moneyscore : sciencescore : concat cardPoints))

-- game part

shuffle :: Eq a => [a] -> GameMonad p [a]
shuffle [] = return []
shuffle lst = do
    r <- singleton (Roll (length lst))
    let mycard = lst !! r
    nxt <- shuffle (filter (/= mycard) lst)
    return (mycard : nxt)

getAgeCards :: Age -> GameMonad p [Carte]
getAgeCards curAge = do
    nbPlayers <- M.size <$> use players
    let ageCards = filter (\c -> c ^. cjoueurs <= nbPlayers && c ^. cage == curAge) allcards
        nbguilds = nbPlayers + 2
    shuffledGuilds <- if curAge == Age3 then shuffle guilds else return []
    shuffle (take nbguilds shuffledGuilds <> ageCards)

dealCards :: Age -> GameMonad p ()
dealCards age = do
    cards <- getAgeCards age
    playerList <- M.keys <$> use players
    let (chunksize, rest) = length cards `quotRem` length playerList
    unless (rest == 0) (throwError ("Bad number of cards: " <> numerical (length cards) <> " cards for " <> numerical (length playerList) <> " players at age " <> pe age <> "."))
    dealtMap .= M.fromList (zip playerList (chunksOf chunksize cards))

ngetter :: Voisin -> Lens' PlayerState PlayerId
ngetter dir = case dir of
                  VGauche -> leftNeigh
                  VDroite -> rightNeigh

getNeighbour :: MConstraint m => PlayerId -> Voisin -> m PlayerId
getNeighbour pid dir = do
    nid <- preuse (players . ix pid . ngetter dir)
    case nid of
        Nothing -> throwError ("Could not find the neighbour of " <> showPlayerId pid)
        Just x -> return x

getPlayerState :: MConstraint m => PlayerId -> m PlayerState
getPlayerState pid = do
    r <- use (players . at pid)
    case r of
        Nothing -> throwError ("Could not find player " <> showPlayerId pid)
        Just x -> return x

getTargetedPlayers :: MConstraint m => PlayerId -> Cible -> m [PlayerState]
getTargetedPlayers pid tgts =
    let getTarget SoiMeme = return pid
        getTarget Droite = getNeighbour pid VDroite
        getTarget Gauche = getNeighbour pid VGauche
    in  mapM (getTarget >=> getPlayerState) tgts

getNbTargets :: MConstraint m => PlayerId -> Condition -> m Integer
getNbTargets _ OccurenceSimple = return 1
getNbTargets pid (ParEtapeMerveille t) = sum . map (fromIntegral . fromEnum . view wonderStage) <$> getTargetedPlayers pid t
getNbTargets pid ParDefaite = sum . map (fromIntegral . length . filter (== Defaite) . view curconflict) <$> getTargetedPlayers pid [Droite,Gauche]
getNbTargets pid (PossedeCarte t colors) = do
    neighcolors <- toListOf (folded . cardsInGame . folded . filtered isTargetable . ccouleur . filtered ( `S.member` colors) ) <$> getTargetedPlayers pid t
    return $ fromIntegral $ length neighcolors

getMoneyEffect :: MConstraint m => PlayerId -> Carte -> m Money
getMoneyEffect pid card = sum <$> mapM rescond (card ^.. ceffet . traverse . _GagneArgent)
    where
        rescond (mny, cond) = ( (mny *) . fromInteger ) <$> getNbTargets pid cond

consumeOlymp :: MConstraint m => PlayerId -> Age -> m ()
consumeOlymp pid age  = players . ix pid . cardsInGame . traverse . ceffet . traverse . _WOlympie . at age .= Nothing

resolveAction :: Age -> PlayerId -> (PlayerAction, Exchange) -> GameMonad p ()
resolveAction age pid (PlayerAction pt cardid, exch) = do
    -- on résoud les échanges
    let prettyExchanges = "Exchanges:" <+> pe (map prettyExchange (M.toList exch))
        prettyExchange (voisin, res) = pe voisin <> pe res
        resourceExchange dir ressources = do
            neighid <- getNeighbour pid dir
            neighstate <- getPlayerState neighid
            myState <- getPlayerState pid
            let neighExchange = availableResources Exchange neighstate
                exchangeCost = getExchangeCost myState dir ressources
            unless (any (ressources `MS.isSubsetOf`) neighExchange) (throwError ("Somebody cheated and asked for more exchanges with his neighbour ! " <> showPlayerId pid <+> prettyExchanges))
            when (exchangeCost > myState ^. curmoney) (throwError (showPlayerId pid <> " doesn't have enough money" <+> prettyExchanges))
            singleton (GeneralMessage (showPlayerId pid <+> "a échangé" <+> pe exchangeCost <+> "avec" <+> showPlayerId neighid <+> "contre" <+> pe ressources))
            players . ix pid     . curmoney -= exchangeCost
            players . ix neighid . curmoney += exchangeCost
    imapMOf_ ifolded resourceExchange exch
    let extraResources = exch ^. folded
    playerCards <- use (dealtMap . ix pid)
    when (cardid >= length playerCards || cardid < 0) (throwError (showPlayerId pid <> " tried to use an invalid card" <+> prettyExchanges))
    let mycard = playerCards !! cardid
        newhand = filter (/= mycard) playerCards
    curstate <- getPlayerState pid
    dealtMap . at pid ?= newhand
    case pt of
        DropCard -> do
            players . ix pid . curmoney += 3
            discarded %= (mycard :)
            singleton (GeneralMessage (showPlayerId pid <+> "a jeté une carte"))
        BuildWonder -> do
            let curstage = curstate ^. wonderStage
                curwonder = curstate ^. wonder
                maxWonderStage = getMaxStage curwonder
            when (curstage == maxWonderStage) $ throwError (showPlayerId pid <> " tried to upgrade a wonder beyond what is permitted" <+> prettyExchanges)
            let nstage = succ curstage
                wondercard = getCivilizationResource (curstate ^. wonder) nstage
            case isPlayable wondercard curstate extraResources of
                Nothing -> throwError (showPlayerId pid <> " tried to cheat and buy a non playable wonder stage " <> shortCard wondercard <+> prettyExchanges)
                Just mcost -> do
                    players . ix pid %= (cardsInGame %~ (wondercard :)) . (wonderStage .~ nstage)
                    players . ix pid . curmoney -= mcost
                    singleton (GeneralMessage (showPlayerId pid <+> "a construit sa merveille: " <+> brackets (pe nstage)))
                    gain <- getMoneyEffect pid wondercard
                    players . ix pid . curmoney += gain
        PlayCard -> do
            case isPlayable mycard curstate extraResources of
                Nothing -> if hasOlympReady curstate age
                               then consumeOlymp pid age
                               else throwError (showPlayerId pid <> " tried to cheat and buy a non playable card " <> shortCard mycard <+> prettyExchanges)
                Just mcost -> players . ix pid . curmoney -= mcost
            players . ix pid . cardsInGame %= (mycard :)
            gain <- getMoneyEffect pid mycard
            let msg = showPlayerId pid <+> "a joué" <+> shortCard mycard <> if gain > 0
                                                                                 then mempty <+> "et a gagné" <+> pe gain
                                                                                 else mempty
            singleton (GeneralMessage msg)
            players . ix pid . curmoney += gain

rotateHands :: MConstraint m => Voisin -> m ()
rotateHands dir = do
    curDeal <- M.toList <$> use dealtMap
    curdir <- M.map (view (ngetter dir)) <$> use players
    let distribute curmap (pid, hand) = case curdir ^. at pid of
                                            Just npid -> return $ curmap & at npid ?~ hand
                                            Nothing   -> throwError ("Could not find the neighbour of " <> showPlayerId pid <> " when rolling hands")
    nextDeal <- foldM distribute mempty curDeal
    dealtMap .= nextDeal

getMilitaryStrength :: MConstraint m => PlayerId -> m Shield
getMilitaryStrength pid = getSum . view (cardsInGame . folded . ceffet . folded . _Militaire . to Sum) <$> getPlayerState pid

solveMilitary :: Age -> GameMonad p ()
solveMilitary age = use players >>= mapM_ solveMilitary' . M.toList
    where
        solveMilitary' (pid, pst) = do
            myStrength <- getMilitaryStrength pid
            nStrength  <- (\x y -> [x,y]) <$> getMilitaryStrength (pst ^. leftNeigh) <*> getMilitaryStrength (pst ^. rightNeigh)
            let tokens = concatMap getTok nStrength
                getTok s | s > myStrength = [Defaite]
                         | s < myStrength = [Victoire age]
                         | otherwise = []
            unless (null tokens) (singleton (GeneralMessage (showPlayerId pid <+> "a reçu les jetons de conflit suivants:" <+> pe tokens)))
            players . ix pid . curconflict %= (tokens ++)

previewCardEffect :: MConstraint m => PlayerId -> Carte -> m CardEffectPreview
previewCardEffect pid card = CardEffectPreview <$> getMoneyEffect pid card <*> (sum . map snd <$> getCardVictory pid card)

checkEndOfTurnActions :: (PlayerId, PlayerState) -> GameMonad p ()
checkEndOfTurnActions (pid, pstate) =
    when (has (playerEffects . _WHalicarnasse) pstate) $ do
        players . ix pid . cardsInGame %= filter (not . has (ceffet . traverse . _WHalicarnasse))
        disc <- use discarded
        if null disc
            then singleton (GeneralMessage (showPlayerId pid <+> "ne peut pas jouer la capacité spéciale de Halicarnasse : pas de cartes disponibles"))
            else do
                disceffect <- mapM (previewCardEffect pid) disc
                singleton (GeneralMessage (showPlayerId pid <+> "va jouer la capacité spéciale de Halicarnasse"))
                idx <- singleton (AskCard pid (zip disc disceffect) "Quelle carte construire gratuitement (Halicarnasse) ?")
                when (idx < 0 || idx >= length disc) (throwError (showPlayerId pid <> " chose an invalid card id"))
                let mycard = disc !! idx
                discarded %= filter (/= mycard)
                players . ix pid . cardsInGame %= (mycard :)
                singleton (GeneralMessage (showPlayerId pid <+> "a sorti" <+> shortCard mycard <+> "de la défausse"))

checkCopyGuildEffect :: GameMonad p ()
checkCopyGuildEffect = do
    plys <- M.keys <$> use players
    forM_ plys $ \pid -> do
        targets <- use (players . ix pid . playerEffects . _WCopieGuilde)
        unless (null targets) $ do
            tstates <- getTargetedPlayers pid targets
            let otherguilds = tstates ^.. traverse . cardsInGame . traverse . filtered (\c -> c ^. ccouleur == Violet)
            if null otherguilds
                then singleton (GeneralMessage (showPlayerId pid <+> "ne peut pas jouer la capacité spéciale de copie de guilde : pas de cartes disponibles"))
                else do
                    effects <- mapM (previewCardEffect pid) otherguilds
                    singleton (GeneralMessage (showPlayerId pid <+> "va jouer la capacité spéciale de copie de guilde."))
                    idx <- singleton (AskCard pid (zip otherguilds effects) "Quelle guilde copier gratuitement ?")
                    when (idx < 0 || idx >= length otherguilds) (throwError (showPlayerId pid <> " chose an invalid card id"))
                    let myguild = otherguilds !! idx
                    players . ix pid . cardsInGame %= (myguild :)
                    singleton (GeneralMessage (showPlayerId pid <+> "a copié cette guilde:" <+> shortCard myguild))

playAge :: Age -> GameMonad p ()
playAge age = do
    dealCards age
    forM_ [1..7] $ \turn -> do
        use id >>= singleton . Inform age turn
        playerlst <- use players
        let playerids = M.keys $ if turn == 7
                                     then M.filter (has (playerEffects . _WJoueDerniereCarte)) playerlst
                                     else playerlst
        pacts <- forM playerids $ \player -> do
            dealt <- use (dealtMap . ix player)
            effects <- mapM (previewCardEffect player) dealt
            prm <- singleton (AskPlayer age player (zip dealt effects) playerlst)
            return (player, prm)
        plyactions <- mapM (\(player,prm) -> singleton (GetPromise prm) >>= \r -> return (player, r)) pacts
        forM_ plyactions $ \(player,r) -> resolveAction age player r
        -- check halicarnasse
        fmap M.toList (use players) >>= mapM_ checkEndOfTurnActions
        when (turn < 6) $ rotateHands $ if age == Age2
                                            then VDroite
                                            else VGauche
        singleton Flush
    -- au dernier tour on jette les cartes restantes
    cardsLeft <- use (dealtMap . traverse)
    dealtMap .= mempty
    discarded %= (cardsLeft ++)
    solveMilitary age
    singleton Flush

rollWonders :: GameMonad p ()
rollWonders = do
    playerList <- (M.keys <$> use players) >>= shuffle
    wonders <- shuffle [RhodesA .. GizehB]
    let leftList = tail $ concat $ repeat playerList
        righList = last playerList : (concat $ repeat playerList)
        mylist = getZipList ((,,,) <$> ZipList playerList <*> ZipList leftList <*> ZipList righList <*> ZipList wonders)
    forM_ mylist $ \(pid, lft, rght, wond) -> players . ix pid .= PlayerState wond 3 [] NotBuilt lft rght [getCivilizationResource wond NotBuilt]

descWonder :: PlayerId -> PlayerState -> GameMonad p ()
descWonder pid stt = do
    let w = stt ^. wonder
        maxstage = getMaxStage w
    singleton (PlayerMessage pid ("Vous jouez" <+> pe w <+> "avec les capacités suivantes:"))
    forM_ [NotBuilt .. maxstage] $ \stage -> singleton (PlayerMessage pid (pe stage <+> longCard (getCivilizationResource w stage)))

playGame :: GameMonad p (M.Map PlayerId (M.Map VictoryCategory Victory))
playGame = do
    mapM_ (singleton . Roll) [1..200]
    rollWonders
    use players >>= mapM_ (uncurry descWonder) . M.toList
    singleton Flush
    mapM_ playAge [Age1, Age2, Age3]
    checkCopyGuildEffect
    victoryScore
