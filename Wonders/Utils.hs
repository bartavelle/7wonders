{-# LANGUAGE TupleSections #-}
module Wonders.Utils where

import Wonders.Types
import Wonders.Cards
import Control.Lens
import Control.Monad
import Data.Set.Lens
import Data.Monoid
import Data.List (nub)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import qualified Data.MultiSet as MS

-- pure part
data ResourceQueryType = Exchange | Own
                       deriving Eq

playerEffects :: Traversal' PlayerState CardEffect
playerEffects = cardsInGame . traverse . ceffet . traverse

-- Vérifie si on a une carte virtuelle "Olympie" contenant l'age voulu
hasOlympReady :: PlayerState -> Age -> Bool
hasOlympReady stt age = has (cardsInGame . folded . ceffet . folded . _WOlympie . ix age) stt

voisinInTarget :: Voisin -> Cible -> Bool
voisinInTarget v c =
    let vc = case v of
                 VDroite -> Droite
                 VGauche -> Gauche
    in  vc `elem` c

isTargetable :: Carte -> Bool
isTargetable c = c ^? cnom . _head /= Just '['

findExchange :: MS.MultiSet Ressource -> M.Map Voisin (S.Set Ressource) -> MS.MultiSet Ressource -> MS.MultiSet Ressource -> [(Exchange, Money)]
findExchange toAcq cheapExchanges = runSearch (MS.toList toAcq)
    where
       cost v r = if S.member r (cheapExchanges ^. ix v) then 1 else 2
       runSearch :: [Ressource] -> MS.MultiSet Ressource -> MS.MultiSet Ressource -> [(Exchange, Money)]
       runSearch [] _ _ = [(mempty, 0)]
       runSearch (t:ts) lp rp =
           let addExchange v (e, m) = (M.insertWith (<>) v (MS.singleton t) e , m + cost v t)
               leftexchange = if MS.member t lp
                                 then map (addExchange VGauche) (runSearch ts (MS.delete t lp) rp)
                                 else []
               rightexchange = if MS.member t rp
                                   then map (addExchange VDroite) (runSearch ts lp (MS.delete t rp))
                                   else []
           in  leftexchange ++ rightexchange

findCheapExchanges :: PlayerState -> M.Map Voisin (S.Set Ressource)
findCheapExchanges ply = M.fromListWith (<>) $ do
    (targets, res) <- ply ^.. playerEffects . _EchangePasCher
    target <- targets
    case target of
        Droite -> return (VDroite, res)
        Gauche -> return (VGauche, res)
        _      -> mempty

getCardActions :: Age -> PlayerState -> [MS.MultiSet Ressource] -> [MS.MultiSet Ressource] -> (Int, Carte) -> [(PlayerAction, Exchange, Maybe SpecialActionInformation)]
getCardActions age playerstate lplayer rplayer (cid, card)
    | view cnom card `S.member` alreadyBuilt = dropaction
    | has _Just (isPlayable card playerstate mempty) = (PlayerAction PlayCard cid, mempty, Nothing) : dropaction
    | otherwise = map (\e -> (PlayerAction PlayCard cid, e, Nothing)) bestExchange ++ dropaction ++ olympbuild
    where
        olympbuild = [(PlayerAction PlayCard cid, mempty, Just UsedOlymp) | hasOlympReady playerstate age ]
        dropaction = [(PlayerAction DropCard cid, mempty, Nothing)]
        alreadyBuilt = setOf (cardsInGame . traverse . cnom) playerstate
        myresources = availableResources Own playerstate
        mymoney = playerstate ^. curmoney
        bestExchange = map fst $ filter ((==leastMoney) . snd) checkExchange
        leastMoney = minimum (map snd checkExchange)
        checkExchange :: [(Exchange, Money)]
        checkExchange = do
            let Cost neededresources neededmoney = card ^. ccost
            guard (mymoney >= neededmoney)
            curresources <- myresources
            let resourcesToAcquire = neededresources `MS.difference` curresources
                cheapExchanges = findCheapExchanges playerstate
            lplayer' <- lplayer
            rplayer' <- rplayer
            (exchange, ecost) <- findExchange resourcesToAcquire cheapExchanges lplayer' rplayer'
            guard (ecost + neededmoney <= mymoney)
            return (exchange, ecost)

allowableActions :: Age -> PlayerId  -> [Carte] -> M.Map PlayerId PlayerState -> [(PlayerAction, Exchange, Maybe SpecialActionInformation)]
allowableActions age pid cards playrs =
    let playerInfos :: Maybe (PlayerState, [MS.MultiSet Ressource], [MS.MultiSet Ressource])
        playerInfos = do
            mpstate <- playrs ^. at pid
            lpstats <- playrs ^. at (view leftNeigh mpstate)
            rpstats <- playrs ^. at (view rightNeigh mpstate)
            return (mpstate, availableResources Exchange lpstats, availableResources Exchange rpstats)
        nbcards = length cards
        cardids = [0 .. (nbcards - 1) ]
    in case playerInfos of
        Just (playerstate, lplayer, rplayer) ->
            let cardactions = concatMap (getCardActions age playerstate lplayer rplayer) (zip [0 ..] cards)
                wstage = playerstate ^. wonderStage
                curwonder = playerstate ^. wonder
                maxstage = getMaxStage curwonder
                wondercard = getCivilizationResource curwonder (succ wstage)
                notDrop (PlayerAction DropCard _) = False
                notDrop _ = True
                wonderactions = filter (\(pa, _, sa) -> has _Nothing sa && notDrop pa) (getCardActions age playerstate lplayer rplayer (0, wondercard))
                mkWonderBuild e = [ (PlayerAction BuildWonder i, e, Nothing) | i <- cardids ]
                wonderaction | wstage == maxstage = []
                             | otherwise = concatMap (mkWonderBuild . view _2) wonderactions
            in  nub (cardactions ++ wonderaction)
        _ -> [ (PlayerAction DropCard i, mempty, Nothing) | i <- cardids ]

isPlayable :: Carte -> PlayerState -> MS.MultiSet Ressource -> Maybe Money
isPlayable card playerstate extraresources
    | (card ^. cnom) `S.member` alreadyPlayed = Nothing
    | S.member (card ^. cnom) freeBuildings = Just 0
    | any (rescost `MS.isSubsetOf`) resources && (moneycost <= playerstate ^. curmoney) = Just moneycost
    | otherwise = Nothing
    where freeBuildings = setOf (cardsInGame . folded . cgratuit . folded) playerstate
          resources = availableResources Own playerstate & map (<> extraresources)
          alreadyPlayed = setOf (cardsInGame . folded . cnom) playerstate
          Cost rescost moneycost = card ^. ccost

availableResources :: ResourceQueryType -> PlayerState -> [MS.MultiSet Ressource]
availableResources rt ps =
    let getSet (GagneRessource r nb t) = [(r, nb) | isApplicable t]
        getSet (ChoixRessource st t) = if isApplicable t
                                           then st ^.. folded . to (\x -> (x,1))
                                           else []
        getSet _ = []
        isApplicable Partage = True
        isApplicable _ = rt == Own
        effects = ps ^.. playerEffects . to getSet . filtered (not . null)
        getComb :: [[(Ressource, Int)]] -> [MS.MultiSet Ressource]
        getComb [] = [mempty]
        getComb (x:xs) = do
            (cv, cn) <- x
            rst <- getComb xs
            return (MS.insertMany cv cn rst)
    in  getComb effects

getExchangeCost :: PlayerState -> Voisin -> MS.MultiSet Ressource -> Money
getExchangeCost ps dir res =
    let cheapExchanges = setOf (playerEffects . _EchangePasCher . filtered (voisinInTarget dir . view _1) . _2 . folded ) ps
        getCost r = if has (ix r) cheapExchanges then 1 else 2
    in sum $ map getCost $ MS.toList res

dummyState :: [PlayerId] -> GameState
dummyState lst = GameState mempty (M.fromList $ map (, dummyPState) lst) []
    where
        dummyPState = PlayerState RhodesA 0 [] NotBuilt "dummy" "dummy" []
