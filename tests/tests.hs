{-# LANGUAGE OverloadedStrings #-}
module Main where

import Wonders.Types
import Wonders.Cards
import Wonders.Game
import Wonders.Pure
import Wonders.Utils
import Text.PrettyPrint.ANSI.Leijen (pretty)

import Data.List (nub)
import Test.Hspec
import Data.Monoid
import Control.Lens
import Data.List (sort,intercalate)
import Test.QuickCheck
import System.Random

getCard :: String -> Carte
getCard n = head $ filter (\c -> c ^. cnom == n) allcards

main :: IO ()
main = hspec $ do
    describe "Cards" $ do
        it "are all distinct" $ allcards `shouldBe` nub allcards
        let nbc age nbplayers = it ("are the correct number for " ++ show age ++ " and " ++ show nbplayers ++ " players") (check age nbplayers)
            check age nbplayers =
                let playrs = map show ( [1..nbplayers] :: [Int] )
                    (_, Right x) = runDummy playrs (getAgeCards age)
                in  length x `shouldBe` nbplayers * 7
        mapM_ (uncurry nbc) [ (age, nbp) | age <- [Age1,Age2,Age3], nbp <- [3 .. 7] ]
    describe "Dummy games" $ do
        let genSeedPlayers = do
                seed <- arbitrary
                nbplayers <- Test.QuickCheck.elements [3 .. 7]
                return (seed, nbplayers)
        it "all end well" $ forAll genSeedPlayers $ \(seed,nplayers) -> let pids = map show ([1 .. nplayers] :: [Int])
                                                                            (_, res) = runPure (mkStdGen seed) (dummyStrategies pids) (dummyState pids) playGame
                                                                        in  case res of
                                                                                Right _ -> True
                                                                                Left r  -> error (show (pretty r))
    describe "Available resources checker" $ do
        let testres cards test = it ("gives the correct count of resources for " ++ intercalate ", " cards) $ do
                let ps = PlayerState RhodesA 0 mempty NotBuilt dummyPlayer dummyPlayer (getCivilizationResource RhodesA NotBuilt : map getCard cards)
                    getCost (Cost c _) = c
                sort (availableResources Own ps) `shouldBe` sort (fmap getCost test)
        testres [] ["M"]
        testres ["Cavité"] ["MP"]
        testres ["Cavité","Fonderie"] ["MPMM"]
        testres ["Friche"] ["MB","MA"]
        testres ["Friche","Mine"] ["MBM","MAM","MBP","MAP"]
    describe "Science score" $ do
        it "squares well 1" $ scienceScore [Ecrou] 0 `shouldBe` 1
        it "squares well 2" $ scienceScore [Ecrou,Ecrou] 0 `shouldBe` 4
        it "squares well 3" $ scienceScore [Ecrou,Ecrou,Ecrou] 0 `shouldBe` 9
        it "adds well 0" $ scienceScore [Ecrou,Tablette,Compas] 0 `shouldBe` 10
        it "adds well 1" $ scienceScore [Ecrou,Ecrou,Tablette,Compas] 0 `shouldBe` 13
        it "works like in the manual" $ scienceScore [Compas,Compas,Compas,Tablette,Tablette,Ecrou] 0 `shouldBe` 21
        it "works like in the manual" $ scienceScore [Compas,Compas,Compas,Tablette,Tablette,Ecrou,Ecrou] 0 `shouldBe` 31
        it "works like in the manual" $ scienceScore [Compas,Compas,Compas,Tablette,Tablette,Ecrou] 1 `shouldBe` 31
        it "chooses well" $ scienceScore [Compas] 2 `shouldBe` 10
        it "chooses well" $ scienceScore [Compas,Compas] 2 `shouldBe` 16

